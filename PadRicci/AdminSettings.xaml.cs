﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PadRicci
{
  /// <summary>
  /// Logica di interazione per AdminSettings.xaml
  /// </summary>
  public partial class AdminSettings : Page
  {

    int NumberOfPages = 4;
    static App App = (System.Windows.Application.Current as App);

    public AdminSettings()
    {
      InitializeComponent();
      tgPage1.IsChecked = true;
      page1.Visibility = System.Windows.Visibility.Visible;
      page2.Visibility = System.Windows.Visibility.Hidden;
      page3.Visibility = System.Windows.Visibility.Hidden;
    }

    private void ToggleButton_Click(object sender, RoutedEventArgs e)
    {
      Button btn = (Button)sender;

      int PageNumber = Convert.ToInt32(txtNum.Tag.ToString());

      if (btn.Tag.ToString() == "P")
      {
        if (PageNumber > 1)
          PageNumber--;
      }

      if (btn.Tag.ToString() == "N")
      {
        if (PageNumber < NumberOfPages)
          PageNumber++;
      }

      txtNum.Tag = PageNumber.ToString();
      txtNum.Text = String.Format("Pagina {0}", PageNumber);

      switch (PageNumber)
      {
        case 1:
          cmdPrevious.IsEnabled = false;
          cmdNext.IsEnabled = true;
          
          page1.Visibility = System.Windows.Visibility.Visible;
          page2.Visibility = System.Windows.Visibility.Hidden;
          page3.Visibility = System.Windows.Visibility.Hidden;
          page4.Visibility = System.Windows.Visibility.Hidden;
          break;
        case 2:
          cmdPrevious.IsEnabled = true;
          cmdNext.IsEnabled = true;

          page2.Visibility = System.Windows.Visibility.Visible;
          page1.Visibility = System.Windows.Visibility.Hidden;
          page3.Visibility = System.Windows.Visibility.Hidden;
          page4.Visibility = System.Windows.Visibility.Hidden;
          break;
        case 3:
          cmdPrevious.IsEnabled = true;
          cmdNext.IsEnabled = true;

          page3.Visibility = System.Windows.Visibility.Visible;
          page1.Visibility = System.Windows.Visibility.Hidden;
          page2.Visibility = System.Windows.Visibility.Hidden;
          page4.Visibility = System.Windows.Visibility.Hidden;
          break;

        case 4:
          cmdPrevious.IsEnabled = true;
          cmdNext.IsEnabled = false;

          page4.Visibility = System.Windows.Visibility.Visible;
          page1.Visibility = System.Windows.Visibility.Hidden;
          page2.Visibility = System.Windows.Visibility.Hidden;
          page3.Visibility = System.Windows.Visibility.Hidden;
          break;
      }
    }
  }
}
