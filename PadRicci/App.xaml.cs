﻿using Impinj.OctaneSdk;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace PadRicci
{
  /// <summary>
  /// Logica di interazione per App.xaml
  /// </summary>
  public partial class App : Application
  {
    public string PasswordToTest = "";

    // {DB - 09/05/2016} Creo un elenco di Tag da scrivere 
    public List<Tag> TagsToWrite { get; set;  }
    public List<Tag> TagsWritten { get; set; }

    public Packing PackingObject;

    public class tagRFID
    {
      public bool LastWrite { get; set; }
      public string EPC { get; set; }
      public string TID { get; set; }
      public string RfidTag { get; set; }
      public bool Processato { get; set; }
      public DateTime TimeStamp { get; set; }
      public string DateTimeString
      {
        get
        {
          return TimeStamp.ToString("dd/MM/yyyy HH:mm:ss");
        }
      }
    }

    public int OldWritingState = -1;
    private int _WritingState = -1;
    public int WritingState
    {
      get
      {
        return _WritingState;
      }
      set
      {
        //OldWritingState = _WritingState;
        _WritingState = value;

      }
    }
    public string RFIDErrorMessage = "";
    public string EPCToWrite;
    public string LastTIDProcessed;
    public bool Rewritetag = false;
    public bool ResetTag = false; // V1.0.3
    public bool SkipPwd = false; // V1.0.3

    public Tag LastTag;
    public int timerCounter = 0;
    //public bool LettureConMemoria = false;

    public ObservableCollection<tagRFID> LocEPCList;
    //    public ObservableCollection<tagRFID> LocEPCListDS;
    public List<tagRFID> LocEPCListWrite;

    public Models.UserSettings UserStg;
    public Models.AdminSettings AdminStg;

    public DataSet dsUserSettings;
    public DataSet dsAdminSettings;

    public Models.Function ActualFunction;

    public Frame MainFrame;

    public WriteTag FormWriteTag;

    public event PropertyChangedEventHandler PropertyChanged;
    private void OnPropertyChanged(String name)
    {
      if (PropertyChanged != null)
      {
        PropertyChanged(this, new PropertyChangedEventArgs(name));
      }
    }

    public void WriteUserSettings()
    {
      // Scrive il file di configurazione su DISCO
      // Aggirono il dataset in memoria e poi salvo il
      // valore su DISCO
      dsUserSettings.Tables[0].Rows[0]["ReadPower"] = UserStg.ReadPower;
      dsUserSettings.Tables[0].Rows[0]["WritePower"] = UserStg.WritePower;
      dsUserSettings.Tables[0].Rows[0]["SoundOK"] = UserStg.SoundOK;
      dsUserSettings.Tables[0].Rows[0]["SoundKO"] = UserStg.SoundKO;
      dsUserSettings.Tables[0].Rows[0]["MantieniTag"] = UserStg.MantieniTag;
      dsUserSettings.AcceptChanges();

      dsUserSettings.WriteXml(
        System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
                "UserSettings.xml"), System.Data.XmlWriteMode.WriteSchema);
    }

    public void ReadUserSettings()
    {
      dsUserSettings = new DataSet();

      dsUserSettings.ReadXml(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
                  "UserSettings.xml"));

      // Leggo le impostazioni UTENTE

      UserStg = new Models.UserSettings();
      UserStg.ReadPower = Convert.ToDouble(dsUserSettings.Tables[0].Rows[0]["ReadPower"]);
      UserStg.WritePower = Convert.ToDouble(dsUserSettings.Tables[0].Rows[0]["WritePower"]);
      UserStg.SoundOK = Convert.ToString(dsUserSettings.Tables[0].Rows[0]["SoundOK"]);
      UserStg.SoundKO = Convert.ToString(dsUserSettings.Tables[0].Rows[0]["SoundKO"]);
      UserStg.MantieniTag = Convert.ToString(dsUserSettings.Tables[0].Rows[0]["MantieniTag"]);

    }

    public void ReadAdminSettings()
    {
      dsAdminSettings = new DataSet();
      dsAdminSettings.ReadXml(
          System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
              "AdminSettings.xml"));

      AdminStg = new Models.AdminSettings();
      AdminStg.Element = dsAdminSettings.Tables[0].Rows[0]["Element"].ToString();
      AdminStg.BankNumber = Convert.ToInt32(dsAdminSettings.Tables[0].Rows[0]["BankNumber"]);
      AdminStg.StartPosition = Convert.ToInt32(dsAdminSettings.Tables[0].Rows[0]["StartPosition"]);
      AdminStg.Length = dsAdminSettings.Tables[0].Rows[0]["Length"].ToString();
      AdminStg.ConvertToDecimal = dsAdminSettings.Tables[0].Rows[0]["ConvertToDecimal"].ToString();
      AdminStg.ReaderMode = dsAdminSettings.Tables[0].Rows[0]["ReaderMode"].ToString();
      AdminStg.BitsNumber = dsAdminSettings.Tables[0].Rows[0]["BitsNumber"].ToString();
      AdminStg.EncodingType = dsAdminSettings.Tables[0].Rows[0]["EncodingType"].ToString();
      AdminStg.WriteMask = dsAdminSettings.Tables[0].Rows[0]["WriteMask"].ToString();

      AdminStg.ReadBytes = Convert.ToInt32(dsAdminSettings.Tables[0].Rows[0]["ReadBytes"]);
      AdminStg.BytesToWrite = Convert.ToInt32(dsAdminSettings.Tables[0].Rows[0]["BytesToWrite"]);

      AdminStg.TagPassword = dsAdminSettings.Tables[0].Rows[0]["TagPassword"].ToString();
      AdminStg.FileLog = dsAdminSettings.Tables[0].Rows[0]["FileLog"].ToString();
      AdminStg.DBLog = dsAdminSettings.Tables[0].Rows[0]["DBLog"].ToString();
      AdminStg.DBLogConnectionString = dsAdminSettings.Tables[0].Rows[0]["DBLogConnectionString"].ToString();
      AdminStg.FilteringTag = dsAdminSettings.Tables[0].Rows[0]["FilteringTag"].ToString();
      AdminStg.FilteringSQL = dsAdminSettings.Tables[0].Rows[0]["FilteringSQL"].ToString();
      AdminStg.FilteringConnectionString = dsAdminSettings.Tables[0].Rows[0]["FilteringConnectionString"].ToString();
      AdminStg.ReadAfterWrite = dsAdminSettings.Tables[0].Rows[0]["ReadAfterWrite"].ToString();

      #region Nuove opzioni v.1.0.3
      if (dsAdminSettings.Tables[0].Columns.IndexOf("AlwaysRewrite") == -1)
        AdminStg.AlwaysRewrite = "N";
      else
        AdminStg.AlwaysRewrite = dsAdminSettings.Tables[0].Rows[0]["AlwaysRewrite"].ToString();

      if (dsAdminSettings.Tables[0].Columns.IndexOf("EnableRewriteOption") == -1)
        AdminStg.EnableRewriteOption = "N";
      else
        AdminStg.EnableRewriteOption = dsAdminSettings.Tables[0].Rows[0]["EnableRewriteOption"].ToString();

      if (dsAdminSettings.Tables[0].Columns.IndexOf("UserPassword") == -1)
        AdminStg.UserPassword= PadRicciConfig.Default.UserPassword;
      else
        AdminStg.UserPassword = dsAdminSettings.Tables[0].Rows[0]["UserPassword"].ToString();

      if (dsAdminSettings.Tables[0].Columns.IndexOf("SupervisorPassword") == -1)
        AdminStg.SupervisorPassword = PadRicciConfig.Default.SupervisorPassword;
      else
        AdminStg.SupervisorPassword = dsAdminSettings.Tables[0].Rows[0]["SupervisorPassword"].ToString();

      if (dsAdminSettings.Tables[0].Columns.IndexOf("AdminPassword") == -1)
        AdminStg.AdminPassword = PadRicciConfig.Default.AdminPassword;
      else
        AdminStg.AdminPassword = dsAdminSettings.Tables[0].Rows[0]["AdminPassword"].ToString();

      if (dsAdminSettings.Tables[0].Columns.IndexOf("ResetTagChar") == -1)
        AdminStg.ResetTagChar = "0";
      else
        AdminStg.ResetTagChar = dsAdminSettings.Tables[0].Rows[0]["ResetTagChar"].ToString();
      #endregion

      #region Nuove opzioni v.1.0.7
      if (dsAdminSettings.Tables[0].Columns.IndexOf("DBInsertCommand") == -1)
        AdminStg.DBInsertCommand = "";
      else
        AdminStg.DBInsertCommand = dsAdminSettings.Tables[0].Rows[0]["DBInsertCommand"].ToString();

      if (dsAdminSettings.Tables[0].Columns.IndexOf("DeviceID") == -1)
        AdminStg.DeviceID = "";
      else
        AdminStg.DeviceID = dsAdminSettings.Tables[0].Rows[0]["DeviceID"].ToString();
      #endregion


      #region Nuove opzioni v.1.0.8
      if (dsAdminSettings.Tables[0].Columns.IndexOf("ConcurrentTag") == -1)
        AdminStg.ConcurrentTag = 1;
      else
      {
        try
        {
          AdminStg.ConcurrentTag = Convert.ToInt32(dsAdminSettings.Tables[0].Rows[0]["ConcurrentTag"]);
        }
        catch
        {
          AdminStg.ConcurrentTag = 1;
        }

      }

      #endregion


      #region Nuove opzioni v.2.0.0
      if (dsAdminSettings.Tables[0].Columns.IndexOf("EnableLinkToMorph") == -1)
        AdminStg.EnableLinkToMorph = "N";
      else
      {
        try
        {
          AdminStg.EnableLinkToMorph = dsAdminSettings.Tables[0].Rows[0]["EnableLinkToMorph"].ToString();
        }
        catch
        {
          AdminStg.EnableLinkToMorph = "N";
        }

      }



      if (dsAdminSettings.Tables[0].Columns.IndexOf("EncodingCategory01") == -1)
        AdminStg.EncodingCategory01 = "";
      else
      {
        try
        {
          AdminStg.EncodingCategory01 = dsAdminSettings.Tables[0].Rows[0]["EncodingCategory01"].ToString();
        }
        catch
        {
          AdminStg.EncodingCategory01 = "";
        }
      }

      if (dsAdminSettings.Tables[0].Columns.IndexOf("EncodingCategory02") == -1)
        AdminStg.EncodingCategory02 = "";
      else
      {
        try
        {
          AdminStg.EncodingCategory02 = dsAdminSettings.Tables[0].Rows[0]["EncodingCategory02"].ToString();
        }
        catch
        {
          AdminStg.EncodingCategory02 = "";
        }
      }


      if (dsAdminSettings.Tables[0].Columns.IndexOf("EncodingCategory03") == -1)
        AdminStg.EncodingCategory03 = "";
      else
      {
        try
        {
          AdminStg.EncodingCategory03 = dsAdminSettings.Tables[0].Rows[0]["EncodingCategory03"].ToString();
        }
        catch
        {
          AdminStg.EncodingCategory03 = "";
        }
      }


      if (dsAdminSettings.Tables[0].Columns.IndexOf("EncodingCategory04") == -1)
        AdminStg.EncodingCategory04 = "";
      else
      {
        try
        {
          AdminStg.EncodingCategory04 = dsAdminSettings.Tables[0].Rows[0]["EncodingCategory04"].ToString();
        }
        catch
        {
          AdminStg.EncodingCategory04 = "";
        }
      }


      if (dsAdminSettings.Tables[0].Columns.IndexOf("EncodingCategory05") == -1)
        AdminStg.EncodingCategory05 = "";
      else
      {
        try
        {
          AdminStg.EncodingCategory05 = dsAdminSettings.Tables[0].Rows[0]["EncodingCategory05"].ToString();
        }
        catch
        {
          AdminStg.EncodingCategory05 = "";
        }
      }


      #endregion

    }

    public void WriteAdminSettings()
    {
      // Scrive il file di configurazione su DISCO
      // Aggiorno il dataset in memoria e poi salvo il
      // valore su DISCO
      dsAdminSettings.Tables[0].Rows[0]["Element"] = AdminStg.Element;
      dsAdminSettings.Tables[0].Rows[0]["BankNumber"] = AdminStg.BankNumber.ToString();
      dsAdminSettings.Tables[0].Rows[0]["StartPosition"] = AdminStg.StartPosition.ToString();
      dsAdminSettings.Tables[0].Rows[0]["Length"] = AdminStg.Length;
      dsAdminSettings.Tables[0].Rows[0]["ConvertToDecimal"] = AdminStg.ConvertToDecimal;
      dsAdminSettings.Tables[0].Rows[0]["ReaderMode"] = AdminStg.ReaderMode;
      dsAdminSettings.Tables[0].Rows[0]["BitsNumber"] = AdminStg.BitsNumber;
      dsAdminSettings.Tables[0].Rows[0]["EncodingType"] = AdminStg.EncodingType;
      dsAdminSettings.Tables[0].Rows[0]["WriteMask"] = AdminStg.WriteMask;
      dsAdminSettings.Tables[0].Rows[0]["ReadBytes"] = AdminStg.ReadBytes.ToString();
      dsAdminSettings.Tables[0].Rows[0]["BytesToWrite"] = AdminStg.BytesToWrite.ToString();
      dsAdminSettings.Tables[0].Rows[0]["TagPassword"] = AdminStg.TagPassword;

      dsAdminSettings.Tables[0].Rows[0]["FileLog"] = AdminStg.FileLog;
      dsAdminSettings.Tables[0].Rows[0]["DBLog"] = AdminStg.DBLog;
      dsAdminSettings.Tables[0].Rows[0]["DBLogConnectionString"] = AdminStg.DBLogConnectionString;
      dsAdminSettings.Tables[0].Rows[0]["FilteringTag"] = AdminStg.FilteringTag;
      dsAdminSettings.Tables[0].Rows[0]["FilteringSQL"] = AdminStg.FilteringSQL;
      dsAdminSettings.Tables[0].Rows[0]["FilteringConnectionString"] = AdminStg.FilteringConnectionString;
      dsAdminSettings.Tables[0].Rows[0]["ReadAfterWrite"] = AdminStg.ReadAfterWrite;

      #region Nuove opzioni v.1.0.3
      if (dsAdminSettings.Tables[0].Columns.IndexOf("AlwaysRewrite") == -1)
        dsAdminSettings.Tables[0].Columns.Add("AlwaysRewrite", typeof(string));
      
      dsAdminSettings.Tables[0].Rows[0]["AlwaysRewrite"] = AdminStg.AlwaysRewrite;

      if (dsAdminSettings.Tables[0].Columns.IndexOf("EnableRewriteOption") == -1)
        dsAdminSettings.Tables[0].Columns.Add("EnableRewriteOption", typeof(string));

      dsAdminSettings.Tables[0].Rows[0]["EnableRewriteOption"] = AdminStg.EnableRewriteOption;

      if (dsAdminSettings.Tables[0].Columns.IndexOf("UserPassword") == -1)
        dsAdminSettings.Tables[0].Columns.Add("UserPassword", typeof(string));

      dsAdminSettings.Tables[0].Rows[0]["UserPassword"] = AdminStg.UserPassword;

      if (dsAdminSettings.Tables[0].Columns.IndexOf("SupervisorPassword") == -1)
        dsAdminSettings.Tables[0].Columns.Add("SupervisorPassword", typeof(string));

      dsAdminSettings.Tables[0].Rows[0]["SupervisorPassword"] = AdminStg.SupervisorPassword;

      if (dsAdminSettings.Tables[0].Columns.IndexOf("AdminPassword") == -1)
        dsAdminSettings.Tables[0].Columns.Add("AdminPassword", typeof(string));

      dsAdminSettings.Tables[0].Rows[0]["AdminPassword"] = AdminStg.AdminPassword;


      if (dsAdminSettings.Tables[0].Columns.IndexOf("ResetTagChar") == -1)
        dsAdminSettings.Tables[0].Columns.Add("ResetTagChar", typeof(string));

      dsAdminSettings.Tables[0].Rows[0]["ResetTagChar"] = AdminStg.ResetTagChar;

      #endregion

      #region Nuove opzioni v.1.0.7
      if (dsAdminSettings.Tables[0].Columns.IndexOf("DBInsertCommand") == -1)
        dsAdminSettings.Tables[0].Columns.Add("DBInsertCommand", typeof(string));

      dsAdminSettings.Tables[0].Rows[0]["DBInsertCommand"] = AdminStg.DBInsertCommand;

      if (dsAdminSettings.Tables[0].Columns.IndexOf("DeviceID") == -1)
        dsAdminSettings.Tables[0].Columns.Add("DeviceID", typeof(string));

      dsAdminSettings.Tables[0].Rows[0]["DeviceID"] = AdminStg.DeviceID;

      #endregion

      #region Nuove opzioni v.1.0.8
      if (dsAdminSettings.Tables[0].Columns.IndexOf("ConcurrentTag") == -1)
        dsAdminSettings.Tables[0].Columns.Add("ConcurrentTag", typeof(int));

      dsAdminSettings.Tables[0].Rows[0]["ConcurrentTag"] = AdminStg.ConcurrentTag;

      #endregion

      #region Nuovi Opzioni v.2.0.0
      if (dsAdminSettings.Tables[0].Columns.IndexOf("EnableLinkToMorph") == -1)
        dsAdminSettings.Tables[0].Columns.Add("EnableLinkToMorph", typeof(string));

      dsAdminSettings.Tables[0].Rows[0]["EnableLinkToMorph"] = AdminStg.EnableLinkToMorph;


      if (dsAdminSettings.Tables[0].Columns.IndexOf("EncodingCategory01") == -1)
        dsAdminSettings.Tables[0].Columns.Add("EncodingCategory01", typeof(string));

      dsAdminSettings.Tables[0].Rows[0]["EncodingCategory01"] = AdminStg.EncodingCategory01;

      if (dsAdminSettings.Tables[0].Columns.IndexOf("EncodingCategory02") == -1)
        dsAdminSettings.Tables[0].Columns.Add("EncodingCategory02", typeof(string));

      dsAdminSettings.Tables[0].Rows[0]["EncodingCategory02"] = AdminStg.EncodingCategory02;

      if (dsAdminSettings.Tables[0].Columns.IndexOf("EncodingCategory03") == -1)
        dsAdminSettings.Tables[0].Columns.Add("EncodingCategory03", typeof(string));

      dsAdminSettings.Tables[0].Rows[0]["EncodingCategory03"] = AdminStg.EncodingCategory03;

      if (dsAdminSettings.Tables[0].Columns.IndexOf("EncodingCategory04") == -1)
        dsAdminSettings.Tables[0].Columns.Add("EncodingCategory04", typeof(string));

      dsAdminSettings.Tables[0].Rows[0]["EncodingCategory04"] = AdminStg.EncodingCategory04;


      #endregion


      dsAdminSettings.AcceptChanges();

      dsAdminSettings.WriteXml(
        System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
                "AdminSettings.xml"), System.Data.XmlWriteMode.WriteSchema);
    }


    /// <summary>
    /// Funzione di scrittura del Log su DB
    /// </summary>
    /// <param name="t"></param>
    public void WriteDBLog(Tag t, string ValEPC)
    {
      if (!String.IsNullOrEmpty(AdminStg.DBLogConnectionString) &&
          !String.IsNullOrEmpty(AdminStg.DBInsertCommand))
      {
        // Apro la connessione OLEDB e poi effettuo l'insert
        NDAL.DBManager dbMan = new NDAL.DBManager();
        dbMan.ConnectionString = AdminStg.DBLogConnectionString;
        dbMan.ProviderType = NDAL.DataProvider.OleDb;
        dbMan.ParamClass = new NDAL.DBManager.NDALParameter(NDAL.DataProvider.OleDb);
        dbMan.Open();

        dbMan.ParamClass.ClearParameters();
        dbMan.ParamClass.SetParameter("P1", DateTime.Now);
        dbMan.ParamClass.SetParameter("P3", t.Epc.ToHexString());
        dbMan.ParamClass.SetParameter("P2", t.Tid.ToHexString());
        dbMan.ParamClass.SetParameter("P4", ValEPC );
        dbMan.ExecuteNonQuery(CommandType.Text, AdminStg.DBInsertCommand);
        dbMan.ParamClass.ClearParameters();

        dbMan.Close();
      }
    }

    /// <summary>
    /// Write Log data into Log File
    /// </summary>
    /// <param name="LogLine">Log data to write</param>
    public void WriteLogWriting(Tag tag, string ValEPC, object CategoryLabel)
    {
      // Scrivo la riga nel file di log (CUMULATIVO)
      string fileName =
        Path.Combine(Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), "LOG"),
                "UPADWriteLog_" + DateTime.Today.ToString("yyyyMMdd") + ".log");
      StreamWriter sw = new StreamWriter(fileName, true);
      string line = "";

      line += String.Format("{0:dd/MM/yyyy HH:mm:ss};", DateTime.Now);
      line += String.Format("{0};", AdminStg.DeviceID);
      line += String.Format("{0};", tag.Tid.ToHexString());
      line += String.Format("{0};", ValEPC);

      if (CategoryLabel != null)
        line += String.Format("{0};", CategoryLabel.ToString());


      sw.WriteLine(line);
      sw.Flush();
      sw.Close();

      // Se è attivo il collecamento con Morph allora creo
      // Un file ad ho per inviarlo al server remoto

      if (AdminStg.EnableLinkToMorph == "Y")
      {

        string pathUpload = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), "UPLOAD");
        if (!Directory.Exists(pathUpload))
          Directory.CreateDirectory(pathUpload);

        string fileNameUload =
           Path.Combine(pathUpload,
                   "UPADWriteLog_" + DateTime.Today.ToString("yyyyMMdd") + "_" + DateTime.Now.Ticks.ToString() + ".log");

        StreamWriter sw2 = new StreamWriter(fileName, true);
        sw2.WriteLine(line);
        sw2.Flush();
        sw2.Close();

      }

    }


    /// <summary>
    /// Write Log data into Log File
    /// </summary>
    /// <param name="LogLine">Log data to write</param>
    public static void WriteLog(string LogLine)
    {
      string pt = Path.Combine(Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), "LOG"));

      // Verifico se esiste la cartella di destinazione
      if (!Directory.Exists(pt))
      {
        Directory.CreateDirectory(pt);
      }

      // Scrivo la riga nel file di log
      string fileName =
        Path.Combine(Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), "LOG"),
                "UPADLog_" + DateTime.Today.ToString("yyyyMMdd") + ".log");
      StreamWriter sw = new StreamWriter(fileName, true);
      string line = String.Format("{0:dd/MM/yyyy HH:mm:ss};{1}", DateTime.Now, LogLine);
      sw.WriteLine(line);
      sw.Flush();
      sw.Close();
    }

    public static DependencyObject GetScrollViewer(DependencyObject o)
    {
      // Return the DependencyObject if it is a ScrollViewer
      if (o is ScrollViewer)
      { return o; }

      for (int i = 0; i < VisualTreeHelper.GetChildrenCount(o); i++)
      {
        var child = VisualTreeHelper.GetChild(o, i);

        var result = GetScrollViewer(child);
        if (result == null)
        {
          continue;
        }
        else
        {
          return result;
        }
      }
      return null;
    }

    public string AssemblyVersion
    {
      get
      {
        Assembly assembly = Assembly.GetExecutingAssembly();
        FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
        return fvi.FileVersion;
      }
    }

    public string DecodeEPC(string epc)
    {
      string app = epc.Replace(" ", "").PadRight(32, '0');
      if (AdminStg.BitsNumber == "96")
        return app.Substring(0,24);

      return app.Substring(0, 32);
    }

    public string AssemblyDirectory
    {
      get
      {
        string codeBase = Assembly.GetExecutingAssembly().CodeBase;
        UriBuilder uri = new UriBuilder(codeBase);
        string path = Uri.UnescapeDataString(uri.Path);
        return System.IO.Path.GetDirectoryName(path);
      }
    }

   
  }
}
