﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace PadRicci
{
  /// <summary>
  /// Logica di interazione per CountTag.xaml
  /// </summary>
  public partial class CountTag : Page
  {
    static App App = (System.Windows.Application.Current as App);

    // Timer conteggio tags
    public static DispatcherTimer tmrRefresh = new DispatcherTimer();

    public CountTag()
    {
      InitializeComponent();
      //AzzeraConteggioTag();

      if (App.UserStg.MantieniTag == "Y")
        swcMantieniTag.IsChecked = true;
      else
        swcMantieniTag.IsChecked = false;

      // {SO} - 05/02/2016
      // Avvio il reader e imposto il writing state a -1 per 
      // indicare che non si è in fase di scrittura
      App.WritingState = -1;
      RFIDMonitor.StartReader();

      tmrRefresh.Interval = TimeSpan.FromMilliseconds(100);
      tmrRefresh.Tick += tmrRefresh_Tick;
      tmrRefresh.Start();
    }

    private void BtnAzzera_Click(object sender, RoutedEventArgs e)
    {
      AzzeraConteggioTag();
    }

    private void AzzeraConteggioTag()
    {
      lblCounter.Text = "0";
      lblCounterEPC.Text = "0";
      App.LocEPCList.Clear();
    }

    void tmrRefresh_Tick(object sender, EventArgs e)
    {
      try
      {
        lblCounter.Text = App.LocEPCList.Count.ToString();
        lblCounterEPC.Text = App.LocEPCList.Select(a => a.EPC).Distinct().Count().ToString();
      }
      catch
      { }
    }

    /*
    private void RefreshSwitchMemoria()
    {
      if (App.LettureConMemoria)
      {
        switchSource.Source = new BitmapImage(new Uri(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\switch_on.png"),UriKind.Absolute));
      }
      else
      {
        switchSource.Source = new BitmapImage(new Uri(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\switch_off.png"), UriKind.Absolute));
      }

    }
    */
   
    private void BtnSwitchMemoria_Click(object sender, RoutedEventArgs e)
    {
      // Inverto il senso dell'interruttore
      //App.LettureConMemoria = !App.LettureConMemoria;
      //RefreshSwitchMemoria();
    }
  }
}
