﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PadRicci
{
  /// <summary>
  /// Logica di interazione per DownloadPage.xaml
  /// </summary>
  public partial class DownloadPage : Window
  {
    static App App = (Application.Current as App);
    private List<Models.DownloadFile> _DownloadFiles;

    public DownloadPage(List<Models.DownloadFile> DownloadFiles)
    {
      _DownloadFiles = DownloadFiles;

      InitializeComponent();
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {

      try
      {
        // Inizio ad effettuare il download dei file

        System.Net.WebClient wb = new System.Net.WebClient();


        if (!Directory.Exists(System.IO.Path.Combine(System.IO.Path.Combine(App.AssemblyDirectory, "temp"))))
        {
          Directory.CreateDirectory(System.IO.Path.Combine(System.IO.Path.Combine(App.AssemblyDirectory, "temp")));
        }

        int count = 1;
        foreach (Models.DownloadFile f in _DownloadFiles)
        {

          string localFile = System.IO.Path.Combine(System.IO.Path.Combine(App.AssemblyDirectory, "temp"), System.IO.Path.GetFileName(f.FileName));

          // Se c'è elimino il file temporaneo
          try
          {
            if (System.IO.File.Exists(localFile))
              System.IO.File.Delete(localFile);
          }
          catch
          { }

          lblFileName.Text = String.Format("Download file {0} - {1}/{2}", System.IO.Path.GetFileName(f.FileName), count, _DownloadFiles.Count);
          wb.DownloadFile(f.FileName, localFile);

          count++;
        }

        // Fine Download. Lancio la conferma WEB
        Utils u = new Utils();
        bool ret = u.ConfirmDownload();

        if (ret)
        {
          // Lancio l'autoaggiornamento dell'applicazione
          Process.Start(System.IO.Path.Combine(App.AssemblyDirectory, "NSWUpdater.exe"));
          try
          {
            Process.GetCurrentProcess().Kill();
          }
          catch
          {
            Environment.Exit(0);
          }
        }
        else
        {
          lblFileName.Text = "Errore durante la conferma del download";
          cmdClose.Visibility = Visibility.Visible;
        }
      }
      catch (Exception ex)
      {
        // Errore durante il proceso di aggiornamento
        lblFileName.Text = "Errore durante l'aggiornamento: " + ex.Message;
        cmdClose.Visibility = Visibility.Visible;
      }

    }

    private void Button_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }
  }
}
