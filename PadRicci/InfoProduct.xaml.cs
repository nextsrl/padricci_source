﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace PadRicci
{
  /// <summary>
  /// Logica di interazione per InfoProduct.xaml
  /// </summary>
  public partial class InfoProduct : Page
  {
    static App App = (System.Windows.Application.Current as App);

    // Timer conteggio tags
    public static DispatcherTimer tmrRefresh = new DispatcherTimer();

    public InfoProduct()
    {
      InitializeComponent();
      Clear();
      //RefreshSwitchMemoria();

      tmrRefresh.Interval = TimeSpan.FromMilliseconds(100);
      tmrRefresh.Tick += tmrRefresh_Tick;
      tmrRefresh.Start();
    }

    void tmrRefresh_Tick(object sender, EventArgs e)
    {
      if (App.LocEPCList.Count > 0)
      {
        BindElement();
      }
      else
      {
        Clear();
      }

    }

    /*
    private void RefreshSwitchMemoria()
    {
      if (App.LettureConMemoria)
      {
        switchSource.Source = new BitmapImage(new Uri(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\switch_on.png"), UriKind.Absolute));
      }
      else
      {
        switchSource.Source = new BitmapImage(new Uri(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\switch_off.png"), UriKind.Absolute));
      }


    }
     * */

    
    private void BtnSwitchMemoria_Click(object sender, RoutedEventArgs e)
    {
      // Inverto il senso dell'interruttore
      //App.LettureConMemoria = !App.LettureConMemoria;
      //RefreshSwitchMemoria();
    }

    public void BindElement()
    {
      imgProduct.Source = new BitmapImage(new Uri(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\fendi.jpg"), UriKind.Absolute));

      txtCode.Text = "C1AA456";
      txtDescrizione.Text = "Borsa in pelle...";
      txtColore.Text = "Neutro";
      txtTaglia.Text = "UNICA";
    }

    public void Clear()
    {
      imgProduct.Source = null;

      txtCode.Text = "";
      txtDescrizione.Text = "";
      txtColore.Text = "";
      txtTaglia.Text = "";

      App.LocEPCList.Clear();
    }

    private void BtnAzzera_Click(object sender, RoutedEventArgs e)
    {
      Clear();
    }

  }
}
