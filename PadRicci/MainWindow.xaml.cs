﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using PadRicci.Views;
using System.IO;

namespace PadRicci
{
  /// <summary>
  /// Logica di interazione per MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    static App App = (System.Windows.Application.Current as App);
    public static DispatcherTimer tmrPurgeTag = new DispatcherTimer();

    public static DispatcherTimer tmrSendLogFiles = new DispatcherTimer();

    public delegate void invokeDelegate();

    public MainWindow()
    {
      InitializeComponent();

      App.TagsToWrite = new List<Impinj.OctaneSdk.Tag>();
      App.TagsWritten = new List<Impinj.OctaneSdk.Tag>();
      
      App.LocEPCList = new ObservableCollection<App.tagRFID>();
      App.LocEPCListWrite = new List<App.tagRFID>();

      App.MainFrame = frmMain;
      
      // Carico l'elenco delle funzioni
      LoadApplicationFunctions();

      // Carico le impostazioni
      ReadXmlSettings();
      
      ConfiguraReader();

      //RFIDMonitor.ReadEPCDataTEST();

      tmrPurgeTag.Interval = TimeSpan.FromMilliseconds(100);
      tmrPurgeTag.Tick += tmrPurgeTag_Tick;
      tmrPurgeTag.Start();

      tmrSendLogFiles.IsEnabled = false;
      tmrSendLogFiles.Interval = TimeSpan.FromMinutes(5);
      //tmrSendLogFiles.Interval = TimeSpan.FromSeconds(5);
      tmrSendLogFiles.Tick += tmrSendLogFiles_Tick;
      tmrSendLogFiles.Start();
      //Monitor.ChangeScreenOrientation();
    }
    
    
    void tmrSendLogFiles_Tick(object sender, EventArgs e)
    {
      if (App.AdminStg.EnableLinkToMorph != "Y")
        return;

      tmrSendLogFiles.Stop();

      try
      {
        // Invio Dati a WebService Remoto

        SendLogFilesToMorph();

      }
      catch
      {
      }

      tmrSendLogFiles.Start();

    }

    private void SendLogFilesToMorph()
    {
      // Recupero l'elenco dei file da inviare
      string pathUpload = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), "UPLOAD");

      string[] FileToSend = Directory.GetFiles(pathUpload, "UPADWriteLog_*.LOG");

      foreach (string File in FileToSend)
      {
        try
        {
          StreamReader sr = new StreamReader(File);
          string FileString = sr.ReadToEnd();
          sr.Close();

          // Invio il file al servizio remoto (in POST come file)
          WebClient client = new WebClient(
             String.Format(
               PadRicciConfig.Default.MorphServiceURL + @"&Action={0}&DeviceID={1}", "WRITELOG", PadRicciConfig.Default.DeviceID));

          client.Method = HttpVerb.POST;
          client.PostData = FileString;
          object res = client.MakeRequest();

          if (res != null)
          {
            if (res.ToString().IndexOf("<Value>OK</Value>") >=0 )
            {
              // Upload andato a buon fine. Quindi elimino il file locale
              System.IO.File.Delete(File);
            }
          }
        }
        catch (Exception ex)
        {
          // Errore durente l'invio del file
          App.WriteLog("ERRORE DURANTE INVIO SCRUTTURE: " + ex.Message);
        }
      }
  

    }

    private void ReadXmlSettings()
    {
      App.ReadUserSettings();
      App.ReadAdminSettings();
      
    }

    void tmrPurgeTag_Tick(object sender, EventArgs e)
    {
      try
      {
        if (App.UserStg.MantieniTag=="N" || frmMain.Tag == "WriteTag")
        {
          DateTime dt = DateTime.Now.AddMilliseconds(PadRicciConfig.Default.msPurgeTag * -1);

          //ObservableCollection<App.tagRFID> listToPurge = App.LocEPCList.Se   .FindAll(a => a.TimeStamp < dt);
          var listToPurge = App.LocEPCList.Where( a => a.TimeStamp < dt);
          List<App.tagRFID> list = listToPurge.ToList();

          for (int i=list.Count-1; i>=0; i--)
            App.LocEPCList.Remove(list[i]);
        }

      }
      catch
      {
      }

    }


    private void ConfiguraReader()
    {
      RFIDMonitor.ConfigReader();
    }

    public List<Models.Function> FuncList = new List<Models.Function>();

    private void LoadApplicationFunctions()
    {

      Models.Function f1 = new Models.Function();
      f1.Position = 0;
      f1.ImageFile = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\Read.png");
      f1.Description = "Lettura";
      f1.Target = "ReadTag";
      f1.AdminAccess = false;
      f1.PasswordAccess = false;
      f1.isEnabled = true;
      FuncList.Add(f1);

      Models.Function f2 = new Models.Function();
      f2.Position = 1;
      f2.ImageFile = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\Count.png");
      f2.Description = "Conteggio";
      f2.Target = "CountTag";
      f2.AdminAccess = false;
      f2.PasswordAccess = false;
      f2.isEnabled = true;
      FuncList.Add(f2);

      Models.Function f3 = new Models.Function();
      f3.Position = 2;
      f3.ImageFile = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\Write.png");
      f3.Description = "Scrittura";
      f3.Target = "WriteTag";
      f3.AdminAccess = false;
      f3.PasswordAccess = false;
      f3.isEnabled = true;
      FuncList.Add(f3);

      Models.Function fip = new Models.Function();
      fip.Position = 3;
      fip.ImageFile = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\packinglist.png");
      fip.Description = "Pack.List";
      fip.Target = "PackingList";
      fip.AdminAccess = false;
      fip.PasswordAccess = false;
      fip.isEnabled = true;
      FuncList.Add(fip);


      Models.Function f50 = new Models.Function();
      f50.Position = 4;
      f50.ImageFile = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\UserSettings.png");
      f50.Description = "Impostaz.";
      f50.AdminAccess = false;
      f50.PasswordAccess = true;
      f50.Target = "UserSettings";
      f50.isEnabled = true;
      FuncList.Add(f50);

      Models.Function f99 = new Models.Function();
      f99.Position = 99;
      f99.ImageFile = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\Settings.png");
      f99.Description = "Conf. Amm.";
      f99.Target = "AdminSettings";
      f99.AdminAccess = true;
      f99.PasswordAccess = false;
      f99.isEnabled = true;
      FuncList.Add(f99);

      lvFunctions.ItemsSource = FuncList.OrderBy(a => a.Position);
    }

    private void lvFunctions_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (e.AddedItems.Count > 0)
      {
        // Disattivo il pulsante
        foreach (var v in FuncList)
        {
          v.isEnabled = true;
        }

        ((Models.Function)e.AddedItems[0]).isEnabled = false;

        // Recupero l'elemento selezionato
        string targetPage = ((Models.Function)e.AddedItems[0]).Target;

        App.ActualFunction = (Models.Function)e.AddedItems[0];

        if (((Models.Function)e.AddedItems[0]).AdminAccess && !String.IsNullOrEmpty(App.AdminStg.AdminPassword))
        {
          frmMain.Source = new Uri("Views/AdminPassword.xaml", UriKind.Relative);
          frmMain.Tag = targetPage;
          App.PasswordToTest = App.AdminStg.AdminPassword;
          
          
        }
        else
        {
          if (((Models.Function)e.AddedItems[0]).PasswordAccess && !String.IsNullOrEmpty(App.AdminStg.UserPassword))
          {
            frmMain.Source = new Uri("Views/AdminPassword.xaml", UriKind.Relative);
            frmMain.Tag = targetPage;
            App.PasswordToTest = App.AdminStg.UserPassword;
          }
          else
          {

            frmMain.Source = new Uri(targetPage + ".xaml", UriKind.Relative);
            frmMain.Tag = targetPage;
          }
        }
        
      }
      lvFunctions.ItemsSource = null;
      lvFunctions.ItemsSource = FuncList.OrderBy(a => a.Position);
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      // Controllo aggiornamenti Applicazione (se disponibile una connessione a internet)
      Utils ut = new Utils();

      List<Models.DownloadFile> FileList = ut.CheckUpdate();

      if (FileList != null)
      {
        if (FileList.Count > 0)
        {
          // Trovati Aggiornamenti.
          // Lancio il download degli stessi
          DownloadPage d = new DownloadPage(FileList);
          d.ShowDialog();
        }
      }
    }
  }


}
