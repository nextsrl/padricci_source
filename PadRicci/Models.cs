﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace PadRicci
{
  public class Models 
  {

    public class Function
    {
      public string ImageFile { get; set; }
      public string Description { get; set; }
      public int Position { get; set; }
      public string Target { get; set; }
      public bool AdminAccess { get; set; }
      public bool PasswordAccess { get; set; }

      public bool isEnabled { get; set; }
      
    }

    public class DownloadFile
    {
      public string FileName { get; set; }
    }

    public class UserSettings
    {
      private double _ReadPower;
      private double _WritePower;
      private string _SoundOK;
      private string _SoundKO;
      private string _MantieniTag;

      public double ReadPower
      {
        get
        {
          return _ReadPower;
        }
        set
        {
          _ReadPower = value;
        }
      }
      public double WritePower
      {
        get
        {
          return _WritePower;
        }
        set
        {
          _WritePower = value;
        }
      }
      public string SoundOK
      {
        get
        {
          return _SoundOK;
        }
        set
        {
          _SoundOK = value;
        }
      }
      public string SoundKO
      {
        get
        {
          return _SoundKO;
        }
        set
        {
          _SoundKO = value;
        }
      }
      public string MantieniTag
      {
        get
        {
          return _MantieniTag;
        }
        set
        {
          _MantieniTag = value;
        }
      }
    }

    // Salvo le impostazione su XML local
    public class AdminSettings
    {
      public string Element { get; set; }
      public int BankNumber { get; set; }
      public int StartPosition { get; set; }
      public string Length { get; set; }
      public string ConvertToDecimal { get; set; }
      public string ReaderMode { get; set; }
      public string BitsNumber { get; set; }
      public string EncodingType { get; set; }
      public string WriteMask { get; set; }
      public int ReadBytes { get; set; }
      public int BytesToWrite { get; set; }
      public string TagPassword { get; set; }
      public string FileLog { get; set; }
      public string DBLog { get; set; }
      public string DBLogConnectionString { get; set; }
      public string DBInsertCommand { get; set; }
      public string FilteringTag { get; set; }
      public string FilteringSQL { get; set; }
      public string ReadAfterWrite { get; set; }
      public string FilteringConnectionString { get; set; }
      public string DeviceID { get; set; }
      
      public string AlwaysRewrite { get; set; } // v1.0.2
      public string EnableRewriteOption { get; set; } // v1.0.2
      public string UserPassword { get; set; } // v1.0.2
      public string SupervisorPassword { get; set; } // v1.0.2
      public string AdminPassword { get; set; } // v1.0.2
      public string ResetTagChar { get; set; } // v1.0.2

      public int ConcurrentTag { get; set; } // v1.0.8

      public string EnableLinkToMorph { get; set; } // v2.0

      public string EncodingCategory01 { get; set; } // v2.0
      public string EncodingCategory02 { get; set; } // v2.0
      public string EncodingCategory03 { get; set; } // v2.0
      public string EncodingCategory04 { get; set; } // v2.0
      public string EncodingCategory05 { get; set; } // v2.0

    }


  }

  public class Packing 
  {
    /*private int _id;
    private string _NumeroDDT;
    private DateTime _DataDDT;
    private DateTime _DataCreazione;

    private int _PezziTotali;
    private int _PezziScatola;
    private int _NumeroScatole;
    private int _NumeroColloAttuale;

    public PropertyChangedEventHandler PropertyChanged;
    
    public void NotifyPropertyChanged(string propertyName)
    {
      if (PropertyChanged != null)
        PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
    }


    
    public string NumeroDDT
    {
      get
      {
        return _NumeroDDT;
      }

      set
      {
        _NumeroDDT = value;
      }

    }
    */

    public int id { get; set; }
    public string NumeroDDT { get; set; }
    public DateTime DataDDT { get; set; }
    public DateTime DataCreazione { get; set; }

    public int PezziTotali { get; set; }
    public int PezziScatola { get; set; }
    public int NumeroScatole { get; set; }
    public int NumeroColloAttuale { get; set; }
    public ObservableCollection<PackingItem> Details { get; set; }

  }

  public class PackingItem
  {
    public int id { get; set; }
    public int IdPacking { get; set; }
    public int NumeroCollo { get; set; }
    public string Tid { get; set; }
    public string Epc { get; set; }

    public string Custom01 { get; set; }
    public string Custom02 { get; set; }
    public string Custom03 { get; set; }
    public string Custom04 { get; set; }
    public string Custom05 { get; set; }
    public string Custom06 { get; set; }
    public string Custom07 { get; set; }
    public string Custom08 { get; set; }
    public string Custom09 { get; set; }
    public string Custom10 { get; set; }

  }
}
