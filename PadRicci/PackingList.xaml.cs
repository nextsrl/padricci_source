﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace PadRicci
{

  /// <summary>
  /// Logica di interazione per PackingList.xaml
  /// </summary>
  public partial class PackingList : Page
  {
    static App App = (System.Windows.Application.Current as App);

    // Timer conteggio tags
    public static DispatcherTimer tmrRefresh = new DispatcherTimer();

    public PackingList()
    {
      InitializeComponent();
      //AzzeraConteggioTag();

      // {SO} - 05/02/2016
      // Avvio il reader e imposto il writing state a -1 per 
      // indicare che non si è in fase di scrittura
      App.WritingState = -1;
      RFIDMonitor.StartReader();



      tmrRefresh.Interval = TimeSpan.FromMilliseconds(100);
      tmrRefresh.Tick += tmrRefresh_Tick;
      tmrRefresh.Start();

      Rebind();
    }

    private void BtnAzzera_Click(object sender, RoutedEventArgs e)
    {
      AzzeraConteggioTag();
    }

    private void AzzeraConteggioTag()
    {
      App.LocEPCList.Clear();
    }

    void tmrRefresh_Tick(object sender, EventArgs e)
    {
      try
      {
      }
      catch
      { }
    }

    /*
    private void RefreshSwitchMemoria()
    {
      if (App.LettureConMemoria)
      {
        switchSource.Source = new BitmapImage(new Uri(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\switch_on.png"),UriKind.Absolute));
      }
      else
      {
        switchSource.Source = new BitmapImage(new Uri(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\switch_off.png"), UriKind.Absolute));
      }

    }
    */

    private void BtnSwitchMemoria_Click(object sender, RoutedEventArgs e)
    {
      // Inverto il senso dell'interruttore
      //App.LettureConMemoria = !App.LettureConMemoria;
      //RefreshSwitchMemoria();
    }

    private void BtnNuovoPacking_Click(object sender, RoutedEventArgs e)
    {

      // Creo il nuova Packing List:
      //App.PackingObject = new Packing();
      //App.PackingObject.NumeroDDT = "DDT1";
      //App.PackingObject.DataDDT = DateTime.Today;

      //Rebind();

      popUpNewPacking.IsOpen = true;
    }

    private void Rebind()
    {
      this.DataContext = null;
      this.DataContext = App.PackingObject;
    }


    private void btnCancel_Click(object sender, RoutedEventArgs e)
    {
      popUpNewPacking.IsOpen = false;
    }


    private void btnOK_Click(object sender, RoutedEventArgs e)
    {
      // Controllo i dati inseriti
      if (txtNumDDT.Text.Length == 0 || txtDataDDT.Text.Length == 0)
      {
        // Mrssaggio di errore




        //popUpNewPacking.IsOpen = false;
        //MessageBox.Show("Inserire il numero e la data del DDT");
        //popUpNewPacking.IsOpen = true;
        return;
      }

      popUpNewPacking.IsOpen = false;

      App.PackingObject = new Packing();
      App.PackingObject.NumeroDDT = txtNumDDT.Text;
      App.PackingObject.DataDDT = txtDataDDT.SelectedDate.Value;

      Rebind();

    }

    private void txtNumDDT_GotFocus(object sender, RoutedEventArgs e)
    {
      ((TextBox)sender).Background = new SolidColorBrush(Color.FromRgb(255, 255, 0));
      ucKeyboard._txInput = (TextBox)sender;
    }

    private void txtNumDDT_LostFocus(object sender, RoutedEventArgs e)
    {
      ((TextBox)sender).Background = new SolidColorBrush(Color.FromRgb(255, 255, 255));
    }

    private void txtDataDDT_GotFocus(object sender, RoutedEventArgs e)
    {
      //((TextBox)sender).Background = new SolidColorBrush(Color.FromRgb(255, 255, 0));
      //ucKeyboard._txInput = (TextBox)sender;
    }

    private void txtDataDDT_LostFocus(object sender, RoutedEventArgs e)
    {
      //((TextBox)sender).Background = new SolidColorBrush(Color.FromRgb(255, 255, 255));
    }
  }
}
