﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Timers;
using Impinj.OctaneSdk;
using System.Diagnostics;
using System.Windows.Threading;
using System.Collections.ObjectModel;
using System.Linq;

namespace PadRicci
{
  public static class RFIDMonitor
  {
    public static bool readerConfigured = false;

    // private  DataSet DsSettings;
    // private  DataRow DrSettingsReader1;

    private static NDAL.DBManager _dbman;
    /*
    static string _AntennaDB;
    static string _ReaderMode;
    static bool _ConvertoToDecimal;
    static bool _FilteringTag = false;
    static string _FilteringSQL = "";
    static string _FilteringConnectionString = "";

    static string _RFIDElement = "";
    static int _RFIDStartPosition = 0;
    static int _RFIDLength = 0;
    
    static string DB = "N";
    */
    static App App = (System.Windows.Application.Current as App);
    static ImpinjReader reader = new ImpinjReader();

    const ushort EPC_OP_ID = 123;
    const ushort PC_BITS_OP_ID = 321;

    public static void ConfigReader()
    {
      LogEvent("Configurazione reader...");

      StartReader();
      readerConfigured = true;
    }

    public static void ChangeAntennaDB(double db)
    {
      if (PadRicciConfig.Default.IPImpinj == "0.0.0.0")
        return;

      Impinj.OctaneSdk.Settings readerSettings = reader.QuerySettings();
      readerSettings.Session = 1;

      readerSettings.Antennas.DisableAll();
      foreach (AntennaConfig Antenna in readerSettings.Antennas)
      {
        Antenna.TxPowerInDbm = App.UserStg.ReadPower;
        Antenna.IsEnabled = true;
      }

      try
      {
        reader.Stop();
      }
      catch
      {
      }

      reader.ApplySettings(readerSettings);

      try
      {
        reader.Start();
      }
      catch
      {
      }

    }

    public static void StartReader()
    {
      string Msg = "";
      try
      {
        if (PadRicciConfig.Default.IPImpinj == "0.0.0.0")
          return;

        reader.Connect(PadRicciConfig.Default.IPImpinj);

        //App.ReadDB = Convert.ToDouble(drSettingsReader["ReadAntennaDB"].ToString());
        //App.WriteDB = Convert.ToDouble(drSettingsReader["WriteAntennaDB"].ToString());

        Impinj.OctaneSdk.Settings readerSettings = reader.QueryDefaultSettings();

        // Tell the reader to include the antenna number
        readerSettings.Report.IncludeAntennaPortNumber = true;
        readerSettings.Report.IncludeFastId = true;
        readerSettings.Report.IncludePcBits = true;
        readerSettings.Report.IncludeChannel = true;
        readerSettings.Report.IncludeDopplerFrequency = true;
        readerSettings.Report.IncludeFirstSeenTime = false;
        readerSettings.Report.IncludeGpsCoordinates = false;
        readerSettings.Report.IncludeLastSeenTime = false;
        readerSettings.Report.IncludePeakRssi = false;
        readerSettings.Report.IncludePhaseAngle = false;
        readerSettings.Report.IncludeSeenCount = false;

        readerSettings.ReaderMode = ReaderMode.AutoSetDenseReader;

        switch (App.AdminStg.ReaderMode)
        {
          case "TAGFOCUS":
            readerSettings.SearchMode = SearchMode.TagFocus;
            break;
          case "SINGLETARGET":
            readerSettings.SearchMode = SearchMode.SingleTarget;
            break;
          case "DUALTARGET":
            readerSettings.SearchMode = SearchMode.DualTarget;
            break;
          default:
            readerSettings.SearchMode = SearchMode.ReaderSelected;
            break;
        }

        readerSettings.Session = 1;

        readerSettings.Antennas.DisableAll();
        foreach (AntennaConfig Antenna in readerSettings.Antennas)
        {
          Antenna.TxPowerInDbm = App.UserStg.ReadPower;
          Antenna.IsEnabled = true;

        }
        reader.ApplySettings(readerSettings);

        // Aggancio l'evento di lettura dei tag
        reader.TagsReported += reader_TagsReported;

        // Avvio il reader
        reader.Start();

        LogEvent(String.Format("{0}>Impinj Reader pronto", reader.Address));
      }
      catch (OctaneSdkException e)
      {
        LogEvent(String.Format("{0}>ERRORE: {1}", reader.Address, e.Message));

        //Console.WriteLine(reader1.Address + ">ERRORE: " + e.Message);
      }
      catch (Exception e)
      {
        LogEvent(String.Format("{0}>ERRORE: {1}", reader.Address, e.Message));
      }
    }

    public static void ReadEPCData()
    {
      reader.TagOpComplete += reader_TagOpComplete;
      TagOpSequence seq = new TagOpSequence();

      TagReadOp readEpc = new TagReadOp();
      readEpc.Id = 99;
      readEpc.MemoryBank = MemoryBank.Epc;
      readEpc.WordPointer = WordPointers.Epc;

      if (App.AdminStg.BitsNumber == "96")
        readEpc.WordCount = 8;

      if (App.AdminStg.BitsNumber == "128")
        readEpc.WordCount = 12;

      //readEpc.WordCount = 8;
      seq.Ops.Add(readEpc);

      reader.AddOpSequence(seq);
    }


    public static string CreateEPCToWrite(string oldEPCTag, string ValToWrite)
    {
      int numCrt = 0;
      if (App.AdminStg.BitsNumber == "96")
        numCrt = 24;

      if (App.AdminStg.BitsNumber == "128")
        numCrt = 32;

      // Converto il codice in base al formato ASCII/HEX o Decimale


      string ValToWriteDef = "";

      if (!String.IsNullOrEmpty(App.AdminStg.WriteMask))
      {

        // Esiste una maschera di scrittura per cui faccio il match tra il valore 
        // Da scrivere e la maschera stessa
        int OrigPos = 0;
        string Mask = App.AdminStg.WriteMask;

        ValToWriteDef = Mask;

        for (int j = 0; j < ValToWriteDef.Length; j++)
        {
          if (ValToWriteDef[j] == '?')
          {
            if (OrigPos < ValToWrite.Length)
            {
              if (j == 0)
              {
                // Primo valore da sostituire
                ValToWriteDef = ValToWrite[OrigPos] + ValToWriteDef.Substring(1);
              }
              else
              {
                if (j == ValToWriteDef.Length - 1)
                {
                  // Ultimo valore da sostituire
                  ValToWriteDef = ValToWriteDef.Substring(0, j) + ValToWrite[OrigPos];
                }
                else
                {
                  // Valore INTERMEDIO da sostituire
                  ValToWriteDef = ValToWriteDef.Substring(0, j) + ValToWrite[OrigPos] + ValToWriteDef.Substring(j + 1);
                }
              }
            }
            OrigPos++;
          }
        }
      }
      else
      {
        ValToWriteDef = ValToWrite;
      }

      // Compenso la lunghezza  in base al numero di caratteri da scrivere
      ValToWriteDef = ValToWriteDef.PadRight(App.AdminStg.BytesToWrite, '0');

      // Paddo il valore in base al tipo di memoria (96bit o 128bit) In  modo da scrivere tutto
      ValToWriteDef = ValToWriteDef.PadRight(numCrt, '0');

      int OrigPos2 = 0;
      string oldEPC = oldEPCTag;
      // Effetto la sostituzione dei caratteri * Con il valore EPC del vecchio tag
      for (int j = 0; j < ValToWriteDef.Length; j++)
      {
        if (ValToWriteDef[j] == '*')
        {
          if (OrigPos2 < ValToWrite.Length)
          {
            if (j == 0)
            {
              // Primo valore da sostituire
              ValToWriteDef = oldEPC[OrigPos2] + ValToWriteDef.Substring(1);
            }
            else
            {
              if (j == ValToWriteDef.Length - 1)
              {
                // Ultimo valore da sostituire
                ValToWriteDef = ValToWriteDef.Substring(0, j) + oldEPC[OrigPos2];
              }
              else
              {
                // Valore INTERMEDIO da sostituire
                ValToWriteDef = ValToWriteDef.Substring(0, j) + oldEPC[OrigPos2] + ValToWriteDef.Substring(j + 1);
              }
            }
          }
          OrigPos2++;
        }
      }


      return ValToWriteDef;
    }

    public static string ConvertASCIIToHex(string InString)
    {
      string OutString = "";

      for (int i = 0; i < InString.Length; i++)
      {
        OutString += Convert.ToByte(InString[i]).ToString("X");
      }
      if (OutString.Length > 32)
        OutString = OutString.Substring(0, 32);


      return OutString;
    }




    /// <summary>
    /// Funzione di scrittura del tag
    /// </summary>
    /// <param name="ValToWrite"></param>
    public static void WriteEPCData(Tag oldTag, string ValToWrite, bool RewritePasswordTag)
    {

      // Prima di lanciare l'operazione cambio la potenza dell'antenna
      ChangeAntennaDB(App.UserStg.WritePower);

      LogEvent("Inizio scrittura TAG: " + ValToWrite);
      App.RFIDErrorMessage = "";

      // Converto la stringa da scrivere nel corretto valore HEX per l'EPC
      ValToWrite = CreateEPCToWrite(oldTag.Epc.ToString().Replace(" ",""), ValToWrite);
      //.PadRight(24, '0');

      if (ValToWrite.IndexOf("?") >= 0)
      {
        // Problemi di configurazione del tag!!!!
        App.RFIDErrorMessage = "Codice da Codificare non valido";
        LogEvent("Codice da Codificare non valido");

        return;
      }

      reader.TagOpComplete += reader_TagOpComplete;

      //      void ProgramEpc(string currentEpc, ushort currentPcBits, string newEpc)

      // Check that the specified EPCs are a valid length
      //if ((currentEpc.Length % 4 != 0) || (newEpc.Length % 4 != 0))
      //        throw new Exception("EPCs must be a multiple of 16 bits (4 hex chars)");
      //
      //     Console.WriteLine("Adding a write operation to change the EPC from :");
      //     Console.WriteLine("{0} to {1}\n", currentEpc, newEpc);


      // Create a tag operation sequence.
      // You can add multiple read, write, lock, kill and QT
      // operations to this sequence.
      TagOpSequence seq = new TagOpSequence();

      // Specify a target tag based on the EPC.
      seq.TargetTag.MemoryBank = MemoryBank.Tid;
      //seq.TargetTag.BitPointer = BitPointers.Epc;
      seq.TargetTag.Data = oldTag.Tid.ToHexString();

      // If you are using Monza 4, Monza 5 or Monza X tag chips,
      // uncomment these two lines. This enables 32-bit block writes
      // which significantly improves write performance.
      //seq.BlockWriteEnabled = true;
      //seq.BlockWriteWordCount = 2;


      // Create a tag write operation to change the EPC.
      TagWriteOp writeEpc = new TagWriteOp();
      // Set an ID so we can tell when this operation has executed.
      writeEpc.Id = EPC_OP_ID;
      // Write to EPC memory
      writeEpc.MemoryBank = MemoryBank.Epc;
      // Specify the new EPC data
      writeEpc.Data = TagData.FromHexString(ValToWrite);
      App.EPCToWrite = ValToWrite;
      // Starting writing at word 2 (word 0 = CRC, word 1 = PC bits)
      writeEpc.WordPointer = WordPointers.Epc;

      
      if ((RewritePasswordTag | App.ResetTag) & !App.SkipPwd)
      {
        // E' un'operazione di riscrittura
        // per cui presuppongo che il tag sia scritto con 
        // password di accesso
        // Aggiungo l'operazione di UNLOCK del banco di memoria

        
        TagLockOp tUnlock = new TagLockOp();
        tUnlock.Id = 90;

        if (!String.IsNullOrEmpty(App.AdminStg.TagPassword))
          tUnlock.AccessPassword = TagData.FromHexString(App.AdminStg.TagPassword);

        tUnlock.TidLockType = TagLockState.None;
        tUnlock.AccessPasswordLockType = TagLockState.Unlock;
        tUnlock.EpcLockType = TagLockState.Unlock;
        tUnlock.KillPasswordLockType = TagLockState.None;
        tUnlock.UserLockType = TagLockState.None;

        seq.Ops.Add(tUnlock);
        reader.AddOpSequence(seq);
        
        
        return;
      }


      if ((RewritePasswordTag | App.ResetTag) & !App.SkipPwd)
      {
        // E' un'operazione di riscrittura
        // per cui presuppongo che il tag sia scritto con 
        // password di accesso
        // Nel caso di reset se ho una pwd settata la imposto V1.0.3
        
        if (!String.IsNullOrEmpty(App.AdminStg.TagPassword))
          writeEpc.AccessPassword = TagData.FromHexString(App.AdminStg.TagPassword);
      }

      // Add this tag write op to the tag operation sequence.
      seq.Ops.Add(writeEpc);

      if (oldTag.Epc.ToHexString().Length != ValToWrite.Length)
      {
        // Adjust the PC bits (4 hex characters per word).
        ushort newEpcLenWords = (ushort)(ValToWrite.Length / 4);
        ushort newPcBits = PcBits.AdjustPcBits(oldTag.PcBits, newEpcLenWords);

        TagWriteOp writePc128 = new TagWriteOp();
        writePc128.Id = PC_BITS_OP_ID;
        // The PC bits are in the EPC memory bank.
        writePc128.MemoryBank = MemoryBank.Epc;
        // Specify the data to write (the modified PC bits).
        writePc128.Data = TagData.FromWord(newPcBits);
        // Start writing at the start of the PC bits.
        writePc128.WordPointer = WordPointers.PcBits;

        // Add this tag write op to the tag operation sequence.
        seq.Ops.Add(writePc128);

      }


      // Se Esiste una password di protezione, aggiungo l'operazione specifica
      // Se sono in reset setto la pwd a null V1.0.3
      if (!String.IsNullOrEmpty(App.AdminStg.TagPassword) || App.ResetTag)
      {
        // Operazione per SETTARE LA PASSWORD
        TagWriteOp writeOp = new TagWriteOp();
        // Assumes that current access password is not set
        // (zero is the default)
        if (RewritePasswordTag && !App.ResetTag)
        {
          // E' un'operazione di riscrittura
          // per cui presuppongo che il tag sia scritto con 
          // passord di accesso
          writeOp.AccessPassword = TagData.FromHexString(App.AdminStg.TagPassword);
        }
        else
        {
          writeOp.AccessPassword = null;
        }
        // The access password is in the Reserved memory bank.
        writeOp.MemoryBank = MemoryBank.Reserved;
        // A pointer to the start of the access password.
        writeOp.WordPointer = WordPointers.AccessPassword;
        // The new access password to write.
        writeOp.Data = TagData.FromHexString(App.AdminStg.TagPassword);

        writeOp.Id = EPC_OP_ID + 1;

        // Add this tag write op to the tag operation sequence.
        seq.Ops.Add(writeOp);

        // Create a tag lock operation to lock EPC memory bank.
        TagLockOp lockUserOp = new TagLockOp();

        // Lock EPC.
        lockUserOp.EpcLockType = TagLockState.Lock;
        lockUserOp.Id = EPC_OP_ID + 2;
        // Add this tag lock op to the tag operation sequence.
        seq.Ops.Add(lockUserOp);

      }

      // Add the tag operation sequence to the reader.
      // The reader supports multiple sequences.
      reader.AddOpSequence(seq);
    }


    /// <summary>
    /// Funzione di scrittura del tag
    /// </summary>
    /// <param name="ValToWrite"></param>
     public static void ReadEPCDataTEST()
    {
      TagOpSequence seq = new TagOpSequence();

      TagReadOp tr = new TagReadOp();
      tr.MemoryBank = MemoryBank.Tid;
      tr.WordCount = 4;
      seq.Ops.Add(tr);
      reader.TagOpComplete += reader_TagOpComplete;

      // Add the tag operation sequence to the reader.
      // The reader supports multiple sequences.
      reader.AddOpSequence(seq);

    }

     static void  reader_TagOpComplete(ImpinjReader reader, TagOpReport report)
    {
      // Reset antenna DB
      ChangeAntennaDB(App.UserStg.ReadPower);

      reader.TagOpComplete -= reader_TagOpComplete;

      // Loop through all the completed tag operations.
      foreach (TagOpResult result in report)
      {
        if (result is TagReadOpResult)
        {
          // Rilettura (readTag)
          if (result.OpId == 99)
          {
            // Operazione di rilettura del tag scritto.
            // Lo verifico con quello da scriver e se ok confermo l'operazione
            // Leggo il tag
            string EPCLetto = result.Tag.Epc.ToHexString();
            if (EPCLetto.Length > App.EPCToWrite.Length)
              EPCLetto = EPCLetto.Substring(0, App.EPCToWrite.Length);

            EPCLetto = EPCLetto.PadRight(App.EPCToWrite.Length, '0');
            if (EPCLetto == App.EPCToWrite)
            {
              successfulWriting(result.Tag, EPCLetto);

              // Lancio l'evento per la visualizzazione dell'ESITO OK

            }
            else
            {
              // ERRORE IN CONTROLLO RILETTURA TAG
              App.WritingState = 2;
              App.RFIDErrorMessage += "Tag scritto erroneamente. Riprovare\n\r";
              if (App.FormWriteTag != null)
              {
                App.FormWriteTag.ViewAlertAsync();
              }

            }

          }
        }

        if (result is TagLockOpResult)
        {
          TagLockOpResult LockResult = result as TagLockOpResult;
          if (LockResult.OpId == EPC_OP_ID + 2 || LockResult.OpId == 90)
          {
            switch (LockResult.Result)
            {
              case LockResultStatus.InsufficientPower:
                App.WritingState = 2;
                App.RFIDErrorMessage += "Potenza di scrittura insufficiente";
                break;
              case LockResultStatus.NonspecificReaderError:
                App.WritingState = 2;
                App.RFIDErrorMessage += "Errore Reader";
                break;
              case LockResultStatus.NonspecificTagError:
                App.WritingState = 2;
                App.RFIDErrorMessage += "Errore TAG";
                break;
              case LockResultStatus.NoResponseFromTag:
                App.WritingState = 2;
                App.RFIDErrorMessage += "Il Tag non risponde";
                break;
              case LockResultStatus.Success:
                break;
            }

            if (App.WritingState == 2)
            {
              if (App.FormWriteTag != null)
              {
                App.FormWriteTag.ViewAlertAsync();
              }
            }
          }
        }

        // Was this completed operation a tag write operation?
        if (result is TagWriteOpResult)
        {
          // Cast it to the correct type.
          TagWriteOpResult writeResult = result as TagWriteOpResult;
          if (1 == 1) //writeResult.OpId == EPC_OP_ID)
          {
            if (writeResult.Result == WriteResultStatus.Success)
            {

              // Se la scrittura è quella dell'EPC lancio una nuova lettura
              // Creo una nuova operazione di lettura
              if (writeResult.OpId == EPC_OP_ID)
              {
                if (App.AdminStg.ReadAfterWrite == "Y")
                {
                  ReadEPCData();
                }
                else
                {
                  // Elimino il tag dalla lista di quelli da codificare
                  foreach (var tag in App.TagsToWrite)
                  {
                    if (tag.Tid.ToHexString() == result.Tag.Tid.ToHexString())
                    {
                      App.TagsWritten.Add(tag);
                      App.TagsToWrite.Remove(tag);
                      break;
                    }
                     
                  }

                  if (App.TagsToWrite.Count == 0)
                  {
                    successfulWriting(result.Tag, App.EPCToWrite);
                  }
                  else
                  {
                    // Rilancio la codifica del prossimo tag
                    WriteEPCData(App.TagsToWrite[0], App.EPCToWrite, App.Rewritetag);
                  }
                }
              }

            }
            else
            {
              App.WritingState = 2;
              switch (writeResult.Result)
              {
                case WriteResultStatus.InsufficientPower:
                  App.RFIDErrorMessage += "Potenza di scrittura insufficiente";
                  break;
                case WriteResultStatus.NonspecificReaderError:
                  if (App.ResetTag) // Sto provando a resettare un tag protetto da pwd quindi riprovo senza
                  {
                    App.SkipPwd = true;
                    App.WritingState = 0; // Resetto lo stato in scrittura e riprovo senza pwd
                  }
                  else
                    App.RFIDErrorMessage += "Errore Reader (probabible password errata)";
                  break;
                case WriteResultStatus.NonspecificTagError:
                  App.RFIDErrorMessage += "Errore TAG";
                  break;
                case WriteResultStatus.NoResponseFromTag:
                  App.RFIDErrorMessage += "Il Tag non risponde";
                  break;
                case WriteResultStatus.TagMemoryLockedError:
                  App.RFIDErrorMessage += "Memoria Tag bloccata";
                  break;
                case WriteResultStatus.TagMemoryOverrunError:
                  App.RFIDErrorMessage += "Memoria Tag insufficiente";
                  break;
              }
              if (App.FormWriteTag != null)
              {
                App.FormWriteTag.ViewAlertAsync();
              }


              // Invoke metodo
            }
            //Console.WriteLine("Write to EPC complete : {0}", writeResult.Result);
          }
          else
          {
            if (writeResult.OpId == PC_BITS_OP_ID)
            {
              //Console.WriteLine("Write to PC bits complete : {0}", writeResult.Result);
            }
          }
          // Print out the number of words written
          // App.RFIDErrorMessage += "Number of words written : " +  writeResult.NumWordsWritten.ToString() + "\n\r";
        }
      }
    }

    private static void successfulWriting(Tag tag, string EPC)
    {
      App.WritingState = 1;
      App.RFIDErrorMessage = "";

      App.ResetTag = false; //V1.0.3
      App.SkipPwd = false; //V1.0.3

      string NewTID = "";

      NewTID = tag.Tid.ToString().Replace(" ", "");
      if (NewTID == "")
        NewTID = "!" + tag.Epc.ToString().Replace(" ", "");

      App.LastTIDProcessed = NewTID;
      App.EPCToWrite = "";

      if (App.LocEPCListWrite.Count > 0)
        App.LocEPCListWrite[0].LastWrite = false;

      App.tagRFID tNew = new App.tagRFID();
      tNew.EPC = EPC;
      tNew.TID = NewTID;

      tNew.TimeStamp = DateTime.Now;
      tNew.LastWrite = true;
      App.LocEPCListWrite.Insert(0, tNew);

      // Rimuovo per avere solo le ultime 50 scritture dei tag
      if (App.LocEPCListWrite.Count > 30)
      {
        try
        {
          App.LocEPCListWrite.RemoveRange(30, App.LocEPCListWrite.Count-30);
        }
        catch
        { }
      }

      foreach (Tag t in App.TagsWritten)
      {
        if (App.AdminStg.FileLog == "Y")
        {

          // Aggiungo Log
          string logLine = "";
          logLine = t.Tid.ToHexString().Replace(" ", "") + ";";
          logLine += App.DecodeEPC(t.Epc.ToHexString());
          App.WriteLog(logLine);

          App.WriteLogWriting(t, tNew.EPC, App.FormWriteTag.cmdActualCategory.Tag);
        }

        if (App.AdminStg.DBLog == "Y")
        {
          // Scrivo su DB
          try
          {
            App.WriteDBLog(t, tNew.EPC);
          }
          catch (Exception ex)
          {
            string logLine2 = "ERROR WRITE TO DB: " + ex.Message;
            App.WriteLog(logLine2);
          }
        }
      }

      App.LastTag = tag;

      if (App.FormWriteTag != null)
      {
        App.FormWriteTag.ViewAlertAsync();
      }
    }

    /// <summary>
    /// Lettura tag RFID e aggiornamento collection in memoria
    /// </summary>
    /// <param name="reader"></param>
    /// <param name="report"></param>
    public static void reader_TagsReported(ImpinjReader reader, TagReport report)
    {
      try
      {
        // Sono in fase di lettura
        if (true) //App.WritingState < 0)
        {
          for (int i = 0; i < report.Tags.Count; i++)
          {
            string tag = App.DecodeEPC(report.Tags[i].Epc.ToString());
            string tid = report.Tags[i].Tid.ToString().Replace(" ", "");
            if (tid == "")
              tid = "!" + tag;

            App.LastTag = report.Tags[i];

            // Scatta l'allarme solo se il TID è NUOVO
            var TagLetti = App.LocEPCList.Where(a => a.TID == tid);
            if (TagLetti.Count() == 0)
            {
              App.tagRFID tNew = new App.tagRFID();
              tNew.EPC = tag;
              tNew.TID = tid;
              tNew.TimeStamp = DateTime.Now;

              /* Decodifico la lettura in base ai parametri */
              /*string app = "";
              if (_RFIDElement == "EPC")
                app = tag;
              else
                app = tid;
              app = app.Substring(_RFIDStartPosition - 1, _RFIDLength);
              if (_ConvertoToDecimal)
                app = int.Parse(app, System.Globalization.NumberStyles.HexNumber).ToString().PadLeft(_RFIDLength, '0');

              tNew.RfidTag = app;
              tNew.TimeStamp = DateTime.Now;
              */

              bool tagOK = true;

              // Verifico inoltre che il tag non sia tra quelli da escludere 
              // perché letto precedentemente

              if (tagOK) // Vuol dire che il tag è già stato letto precedentemente
              {
                tNew.Processato = false;
                App.LocEPCList.Add(tNew);

                LogEvent(String.Format("{0}>{1:HH:mm:ss} + Tag EPC: {2} - TID:{3} Angle: {4} PeakRssiInDbm: {5}", reader.Address, DateTime.Now, tNew.EPC, tNew.TID, report.Tags[i].PhaseAngleInRadians, report.Tags[i].PeakRssiInDbm));
              }
            }
            else
            {
              // Tag sempre presente. Ne aggiorno la data di lettura
              foreach (App.tagRFID t in App.LocEPCList.Where(a => a.TID == tid))
              {
                t.TimeStamp = DateTime.Now;
              }
            }
          }
        }

        // Se sono in fase di scrittura devo leggere il tag sul piano
        if (App.WritingState == 0)
        {
          // {DB - 09/05/2016}
          // Aggiungo il tag nella collection di quelli da scrivere
          if (!App.TagsToWrite.Exists(t => t.Tid.ToString() == report.Tags[0].Tid.ToString()))
          {
            // Aggiungo il tag nella collection
            App.TagsToWrite.Add(report.Tags[0]);
          }

          // La scrittura INIZIA SOLO una volta letti tutti i tag da codificare
          if (App.TagsToWrite.Count() == App.AdminStg.ConcurrentTag)
          {
            App.WritingState = 99; // In attesa di completamento della scrittura

            string NewTID;
            NewTID = report.Tags[0].Tid.ToString().Replace(" ", "");
            if (NewTID == "")
              NewTID = "!" + report.Tags[0].Epc.ToString().Replace(" ", "");

            // Se il tag letto è diverso dall'ultimo scritto
            // o ho abilitato la riscrittura del tag
            // allora procedo alla scrittura
            if (NewTID != App.LastTIDProcessed || App.Rewritetag)
            {
              //WriteEPCData(report.Tags[0], App.EPCToWrite, App.Rewritetag);
              // Lancio le sequenze di scrittura (dal primo tag all'ultimo)
              WriteEPCData(App.TagsToWrite[0], App.EPCToWrite, App.Rewritetag);
            }
            else
            {
              App.WritingState = 2;
              App.RFIDErrorMessage = "TAG Appena processato.";
              if (App.FormWriteTag != null)
              {
                App.FormWriteTag.ViewAlertAsync();
              }

            }

          }
          else
          {
            if (App.TagsToWrite.Count > App.AdminStg.ConcurrentTag)
            {
              // Ho più di un Tag presente sul piano
              App.WritingState = 2;
              App.RFIDErrorMessage = String.Format("AFFOLLAMENTO TAG \n Trovati: ({0})", App.LocEPCList.Count);
              if (App.FormWriteTag != null)
              {
                App.FormWriteTag.ViewAlertAsync();
              }
            }

          }
        }


      }
      catch (Exception ex)
      {
        LogEvent(String.Format("{0}>ERRORE: {1}", reader.Address, ex.Message));
        App.WritingState = 2;
        App.RFIDErrorMessage = String.Format("{0}>ERRORE: {1}", reader.Address, ex.Message);
        if (App.FormWriteTag != null)
        {
          App.FormWriteTag.ViewAlertAsync();
        }

      }
    }

    public static void ResetTagsRead()
    {
      App.LocEPCList.Clear();
    }

    private static void LogEvent(string Msg)
    {
      Debug.Print(Msg);

      string fileName = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), "UPADLog_" + DateTime.Today.ToString("yyyyMMdd") + ".log");
      StreamWriter sw = new StreamWriter(fileName, true);
      string line = String.Format("{0:dd/MM/yyyy HH:mm:ss} - {1}", DateTime.Now, Msg);
      sw.WriteLine(line);
      sw.Flush();
      sw.Close();

      /*
       * if (DB == "Y")
      {
        try
        {
          string sql = " INSERT INTO TagDetail_Log (TimeStamp,EPC,TID) " +
                       " VALUES (:TIMESTAMP,  :EPC,  :TID ); ";

          _dbman.ParamClass.ClearParameters();
          _dbman.ParamClass.SetParameter("TIMESTAMP", DateTime.Now);
          _dbman.ParamClass.SetParameter("EPC", EPC);
          _dbman.ParamClass.SetParameter("TID", TID);
          _dbman.ExecuteNonQuery(CommandType.Text, sql);
          _dbman.ParamClass.ClearParameters();

        }
        catch (Exception ex)
        {
          Console.WriteLine("ERROR: " + ex.Message);
        }
      }
     */
    }

    public static void StopReader()
    {
      try
      {
        reader.Stop();  // Stop reading.                 
        reader.Disconnect(); // Disconnect from the reader.
        LogEvent(String.Format("{0}>Reader disconnesso", reader.Address));
      }
      catch (OctaneSdkException e)
      {
        LogEvent(String.Format("{0}>ERRORE: {1}", reader.Address, e.Message));
      }
      catch (Exception e)
      {
        LogEvent(String.Format("{0}>ERRORE: {1}", reader.Address, e.Message));
      }
    }
  }
}
