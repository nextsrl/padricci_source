﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace PadRicci
{
  /// <summary>
  /// Logica di interazione per ReadTag.xaml
  /// </summary>
  public partial class ReadTag : Page
  {
    static App App = (System.Windows.Application.Current as App);
    public static DispatcherTimer tmrRefresh = new DispatcherTimer();

    public ReadTag()
    {
      InitializeComponent();

      if (App.UserStg.MantieniTag == "Y")
        swcMantieniTag.IsChecked = true;
      else
        swcMantieniTag.IsChecked = false;

      //AzzeraConteggioTag();
      //RefreshSwitchMemoria();

      lvTagLetti.ItemsSource = App.LocEPCList.OrderBy(a => a.EPC);

      // {SO} - 05/02/2016
      // Avvio il reader e imposto il writing state a -1 per 
      // indicare che non si è in fase di scrittura
      App.WritingState = -1;
      RFIDMonitor.StartReader();

      tmrRefresh.Interval = TimeSpan.FromMilliseconds(300);
      tmrRefresh.Tick += tmrRefresh_Tick;
      tmrRefresh.Start();
    }

    void BtnUP_Click(object sender, RoutedEventArgs e)
    {
      var scrollViewer = App.GetScrollViewer(lvTagLetti) as ScrollViewer;
      if (scrollViewer != null)
      {
        scrollViewer.PageUp();
      }
    }

    void BtnDOWN_Click(object sender, RoutedEventArgs e)
    {
      var scrollViewer = App.GetScrollViewer(lvTagLetti) as ScrollViewer;
      if (scrollViewer != null)
      {
        scrollViewer.PageDown();
      }
    }

    private void BtnAzzera_Click(object sender, RoutedEventArgs e)
    {
      AzzeraConteggioTag();
    }

    private void AzzeraConteggioTag()
    {
      lblCounter.Text = "0";
      lblCounterEPC.Text = "0";
      App.LocEPCList.Clear();
      try
      {
        lvTagLetti.ItemsSource = App.LocEPCList.OrderBy(a => a.EPC);
      }
      catch
      { }
    }

    void tmrRefresh_Tick(object sender, EventArgs e)
    {
      
      tmrRefresh.IsEnabled = false;
      try
      {
        lblCounter.Text = App.LocEPCList.Count.ToString();
        lblCounterEPC.Text = App.LocEPCList.Select(a => a.EPC).Distinct().Count().ToString();

        lvTagLetti.ItemsSource = App.LocEPCList.OrderBy(a => a.EPC);
      }
      catch
      { }
      tmrRefresh.IsEnabled = true;
    }

    /*
    private void RefreshSwitchMemoria()
    {
      if (App.LettureConMemoria)
      {
        switchSource.Source = new BitmapImage(new Uri(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\switch_on.png"), UriKind.Absolute));
      }
      else
      {
        switchSource.Source = new BitmapImage(new Uri(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\switch_off.png"), UriKind.Absolute));
      }


    }
    */

    private void BtnSwitchMemoria_Click(object sender, RoutedEventArgs e)
    {
      // Inverto il senso dell'interruttore
      //App.LettureConMemoria = !App.LettureConMemoria;
      //RefreshSwitchMemoria();
    }

    private void ListViewItem_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
     
    }

    private void KeyboardButton_Click(object sender, RoutedEventArgs e)
    {
      // Funzione di gestione della tastiera
      Button btn = (Button)sender;

      if (btn.Tag != null)
      {
        string tag = btn.Tag.ToString();

        switch (tag)
        {
          case "OK":
            // Chiudo il popup
            popUp.IsOpen = false;
            break;
        }
      }
    }

    private void lvTagLetti_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
//      var item = sender as ListViewItem;
      var item = (sender as ListView).SelectedItem;
      if (item != null)
      {
        popUp.IsOpen = true;

        string TID = ((App.tagRFID)item).TID;
        string EPC = ((App.tagRFID)item).EPC;

        txtTID.Text = TID;
        txtEPC.Text = EPC;
      }

    }
  }
}
