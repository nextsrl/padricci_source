﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;

namespace PadRicci
{
  /// <summary>
  /// Logica di interazione per Keyboard.xaml
  /// </summary>
  /// 

  public partial class ScreenKeyboard : System.Windows.Controls.UserControl
  {
    public System.Windows.Controls.TextBox _txInput;
    public System.Windows.Controls.Button _btInput;

    public ScreenKeyboard()
    {
      InitializeComponent();
    }

    public void Show()
    {
      if (_txInput != null)
        _txInput.Background = new SolidColorBrush(Color.FromArgb(0, 255, 0, 0));

      ucKeyboard.Visibility = System.Windows.Visibility.Visible;
    }

    public void Hide()
    {
      if (_txInput != null)
        _txInput.Background = new SolidColorBrush(Color.FromArgb(0, 255, 255, 255));

      _txInput = null;
      _btInput = null;

      ucKeyboard.Visibility = System.Windows.Visibility.Hidden;
    }

    private void Send(Key key)
    {
      if (Keyboard.PrimaryDevice != null)
      {
        if (Keyboard.PrimaryDevice.ActiveSource != null)
        {
          var e = new System.Windows.Input.KeyEventArgs(Keyboard.PrimaryDevice, Keyboard.PrimaryDevice.ActiveSource, 0, key)
          {
            RoutedEvent = Keyboard.PreviewKeyDownEvent
          };
          InputManager.Current.ProcessInput(e);

          // Note: Based on your requirements you may also need to fire events for:
          // RoutedEvent = Keyboard.PreviewKeyDownEvent
          // RoutedEvent = Keyboard.KeyUpEvent
          // RoutedEvent = Keyboard.PreviewKeyUpEvent
        }
      }
    }

    private void KeyboardButton_Click(object sender, RoutedEventArgs e)
    {
      // Funzione di gestione della tastiera
      System.Windows.Controls.Button btn = (System.Windows.Controls.Button)sender;

      if (btn.Tag != null)
      {
        string tag = btn.Tag.ToString();


        if (_txInput != null)
        {
          switch (tag)
          {
            case "BS":
              int selStartDel = _txInput.SelectionStart;
              if (selStartDel > 0)
              {
                _txInput.Text = _txInput.Text.Remove(selStartDel - 1, 1);
                _txInput.SelectionStart = selStartDel - 1;
                //_txInput.Text = _txInput.Text.Substring(0, _txInput.Text.Length - 1);
              }
              break;
            case "CLEFT":
              int selStartLeft = _txInput.SelectionStart;
              if (selStartLeft > 0)
              {
                _txInput.SelectionStart = selStartLeft - 1;
              }
              break;
            case "CRIGHT":
              int selStartRight = _txInput.SelectionStart;
              if (selStartRight < _txInput.Text.Length)
              {
                _txInput.SelectionStart = selStartRight + 1;
              }
              break;

            default:
              int selStart = _txInput.SelectionStart;
              _txInput.Text = _txInput.Text.Insert(_txInput.SelectionStart, tag);
              _txInput.SelectionStart = selStart + 1;
              break;
          }

        }

        if (_btInput != null)
        {
          switch (tag)
          {
            case "BS":
              if (_btInput.Content.ToString().Length > 1)
              {
                _btInput.Content = _btInput.Content.ToString().Substring(0, _btInput.Content.ToString().Length - 1);
              }
              break;

            default:
              _btInput.Content = _btInput.Content.ToString() + tag;
              break;
          }
        }
      }

    }
  }
}
