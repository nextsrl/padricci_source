﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PadRicci
{
  /// <summary>
  /// Logica di interazione per Settings.xaml
  /// </summary>
  public partial class UserSettings : Page
  {
    static App App = (System.Windows.Application.Current as App);

    public UserSettings()
    {
      InitializeComponent();

      lblReadDB.Text = App.UserStg.ReadPower.ToString("0.0");
      lblWriteDB.Text = App.UserStg.WritePower.ToString("0.0");

      swcSoundOK.IsChecked = (App.UserStg.SoundOK == "Y" ? true : false);
      swcSoundKO.IsChecked = (App.UserStg.SoundKO == "Y" ? true : false);
      swcMantieniTag.IsChecked = (App.UserStg.MantieniTag == "Y" ? true : false);



    }

    private void swcChecked(object sender, RoutedEventArgs e)
    {
      ToggleSwitch.HorizontalToggleSwitch ts = (ToggleSwitch.HorizontalToggleSwitch)sender;

      switch (ts.Tag.ToString())
      {
        case "SOUNDOK":
          if (ts.IsChecked)
            App.UserStg.SoundOK = "Y";
          else
            App.UserStg.SoundOK = "N";
          break;
        case "SOUNDKO":
          if (ts.IsChecked)
            App.UserStg.SoundKO = "Y";
          else
            App.UserStg.SoundKO = "N";
          break;
        case "MANTIENITAG":
          if (ts.IsChecked)
            App.UserStg.MantieniTag = "Y";
          else
            App.UserStg.MantieniTag = "N";
          break;
      }
      App.WriteUserSettings();

    }


    private void BtnReadDB_Click(object sender, RoutedEventArgs e)
    {
      double inc = 0;

      if (((Button)sender).Tag.ToString() == "+")
        inc = 1;
      else
        inc = -1;

      double NewDB = App.UserStg.ReadPower + inc;

      if (NewDB < 0)
        NewDB = 0;

      if (NewDB > 30)
        NewDB = 30;

      lblReadDB.Text = NewDB.ToString("0.0");
      App.UserStg.ReadPower = NewDB;
      App.WriteUserSettings();

      // Applico la potenza subito al lettore
      //RFIDMonitor.reader.Stop();
      try
      {
        RFIDMonitor.ChangeAntennaDB(App.UserStg.ReadPower);
      }
      catch
      { 
      }
      //RFIDMonitor.reader.Start();
    }

    private void BtnWriteDB_Click(object sender, RoutedEventArgs e)
    {
      double inc = 0;

      if (((Button)sender).Tag.ToString() == "+")
        inc = 1;
      else
        inc = -1;

      double NewDB = App.UserStg.WritePower + inc;

      if (NewDB < 0)
        NewDB = 0;

      if (NewDB > 30)
        NewDB = 30;

      lblWriteDB.Text = NewDB.ToString("0.0");
      App.UserStg.WritePower = NewDB;
      App.WriteUserSettings();
    }

  }
}
