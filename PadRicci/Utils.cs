﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace PadRicci
{
  public class Utils
  {

    public List<Models.DownloadFile> CheckUpdate()
    {
      try
      {
        // Funzione di check e scaricamento aggiornamenti 
        WebClient client = new WebClient(
                String.Format(
                  PadRicciConfig.Default.MorphServiceURL + @"&Action={0}&DeviceID={1}", "LITE_CHECKDOWNLOAD", PadRicciConfig.Default.DeviceID));

        client.Method = HttpVerb.GET;
        object res = client.MakeRequest();

        // Verifico se è un file JSON Valido nel caso lo analizzo
        try
        {


          List<Models.DownloadFile> FileLists = JsonConvert.DeserializeObject<List<Models.DownloadFile>>(res.ToString());

          // Analizzo la stringa di ritorno
          if (FileLists.Count == 0)
          {
            // Nessun aggiornamento disponibile
            return FileLists;
          }

          // Esiste un aggiornamento
          return FileLists;



        }
        catch
        {
          // Errore in fase di pasing
          return null;
        }

        /*
          UnitaDiMisura[] obj = JsonConvert.DeserializeObject<UnitaDiMisura[]>(JSONResponse);
          if (obj.Length > 0)
          {
            retval = obj[0].siglaunitmis.ToString();
          }
          */
      }
      catch
      {
        return null;
      }
    }


    public bool ConfirmDownload()
    {
      try
      {
        // Funzione di check e scaricamento aggiornamenti 
        WebClient client = new WebClient(
              String.Format(
                PadRicciConfig.Default.MorphServiceURL + @"&Action={0}&DeviceID={1}", "LITE_CONFIRMDOWNLOAD", PadRicciConfig.Default.DeviceID));

        client.Method = HttpVerb.GET;
        client.MakeRequest();

        return true;

      }
      catch
      {
        // Errore in fase di pasing
        return false;
      }
    }

  }
}
