﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PadRicci.Views
{
  /// <summary>
  /// Logica di interazione per AdminPassword.xaml
  /// </summary>
  public partial class AdminPassword : Page
  {
    static App App = (System.Windows.Application.Current as App);
    private string PasswordToCheck = "";

    public AdminPassword()
    {
      InitializeComponent();
      
      var a = this.Parent;
    }

    private void KeyboardButton_Click(object sender, RoutedEventArgs e)
    {
      // Funzione di gestione della tastiera
      System.Windows.Controls.Button btn = (System.Windows.Controls.Button)sender;

      if (btn.Tag != null)
      {
        string tag = btn.Tag.ToString();

        switch (tag)
        {
          case "X":
            txtPassword.Password = "";
            txtErrorMessage.Text = "";
            break;
          case "OK":
            // Controllo la password inserita
            if (txtPassword.Password == App.PasswordToTest)
            {
              // Vado alla pagina corretta
              string targetPage = App.ActualFunction.Target;
              App.MainFrame.Source = new Uri(targetPage + ".xaml", UriKind.Relative);
            }
            else
            {
              txtErrorMessage.Text = "Password errata!";
            }
            break;
          default:
            txtPassword.Password += btn.Tag.ToString();
            break;
        }
      }
    }

    private void Page_GotFocus(object sender, RoutedEventArgs e)
    {
    
    }

   
  }
 }
