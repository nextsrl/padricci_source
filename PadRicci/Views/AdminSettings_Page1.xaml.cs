﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PadRicci.Views
{
  /// <summary>
  /// Logica di interazione per AdminSettings_Page1.xaml
  /// </summary>
  public partial class AdminSettings_Page1 : System.Windows.Controls.UserControl
  {
    static App App = (System.Windows.Application.Current as App);

    public AdminSettings_Page1()
    {
      InitializeComponent();

      // Imposto i valori a video
      if (App.AdminStg.BitsNumber == "96")
        btnBitNumber96.IsChecked = true;

      if (App.AdminStg.BitsNumber == "128")
        btnBitNumber128.IsChecked = true;


      if (App.AdminStg.EncodingType == "ASCII")
        btnEncodingAscii.IsChecked = true;

      if (App.AdminStg.EncodingType == "HEX")
        btnEncodingHex.IsChecked = true;

      if (App.AdminStg.EncodingType == "DECIMAL")
        btnEncodingDecimal.IsChecked = true;


      txtTagPassword.Text = App.AdminStg.TagPassword;
      txtWriteMask.Text = App.AdminStg.WriteMask;

      lblByteBarcode.Text = App.AdminStg.ReadBytes.ToString();
      lblByteToWrite.Text = App.AdminStg.BytesToWrite.ToString();

      lblConcurrentTag.Text = App.AdminStg.ConcurrentTag.ToString();

    }

    private void RadioButton_Checked(object sender, RoutedEventArgs e)
    {
      // Gestisco gli eventi di per gli oggetti RadioButton

      RadioButton rb = (RadioButton)sender;

      switch (rb.GroupName)
      {
        case "BITSNUMBER":
          if (rb.Content.ToString().ToUpper() == "96BIT")
          {
            App.AdminStg.BitsNumber = "96";
          }
          if (rb.Content.ToString().ToUpper() == "128BIT")
          {
            App.AdminStg.BitsNumber = "128";
          }
          break;

        case "ENCODING":
          if (rb.Content.ToString().ToUpper() == "ASCII")
          {
            App.AdminStg.EncodingType = "ASCII";
          }
          if (rb.Content.ToString().ToUpper() == "HEX")
          {
            App.AdminStg.BitsNumber = "HEX";
          }
          if (rb.Content.ToString().ToUpper() == "DECIMALE")
          {
            App.AdminStg.BitsNumber = "DECIMAL";
          }
          break;

      }

      App.WriteAdminSettings();
    }

    
    private void ConcurrentTag_Click(object sender, RoutedEventArgs e)
    {
      Button b = (Button)sender;

      if (b.Tag.ToString() == "+")
      {
        if (App.AdminStg.ConcurrentTag < 40)
          App.AdminStg.ConcurrentTag++;

        lblConcurrentTag.Text = App.AdminStg.ConcurrentTag.ToString();
      }

      if (b.Tag.ToString() == "-")
      {
        if (App.AdminStg.ConcurrentTag > 1)
        {
          App.AdminStg.ConcurrentTag--;
          lblConcurrentTag.Text = App.AdminStg.ConcurrentTag.ToString();
        }
      }
      App.WriteAdminSettings();
    }


    private void ByteToWrite_Click(object sender, RoutedEventArgs e)
    {
      Button b = (Button)sender;

      if (b.Tag.ToString() == "+")
      {
        int max = 0;
        if (App.AdminStg.BitsNumber == "96")
          max = 24;

        if (App.AdminStg.BitsNumber == "128")
          max = 32;

        if (App.AdminStg.BytesToWrite < max)
          App.AdminStg.BytesToWrite++;
        else
          App.AdminStg.BytesToWrite = max;

        lblByteToWrite.Text = App.AdminStg.BytesToWrite.ToString();
      }

      if (b.Tag.ToString() == "-")
      {
        if (App.AdminStg.BytesToWrite > 1)
        {
          App.AdminStg.BytesToWrite--;
          lblByteToWrite.Text = App.AdminStg.BytesToWrite.ToString();
        }
      }
      App.WriteAdminSettings();
    }

    private void BarcodeLength_Click(object sender, RoutedEventArgs e)
    {
      Button b = (Button)sender;

      if (b.Tag.ToString() == "+")
      {
        int max = 0;
        if (App.AdminStg.BitsNumber == "96")
          max = 24;

        if (App.AdminStg.BitsNumber == "128")
          max = 32;

        if (App.AdminStg.ReadBytes < max)
          App.AdminStg.ReadBytes++;
        else
          App.AdminStg.ReadBytes = max;

        lblByteBarcode.Text = App.AdminStg.ReadBytes.ToString();
      }

      if (b.Tag.ToString() == "-")
      {
        if (App.AdminStg.ReadBytes > 1)
        {
          App.AdminStg.ReadBytes--;
          lblByteBarcode.Text = App.AdminStg.ReadBytes.ToString();
        }
      }

      App.WriteAdminSettings();
    }

    private void txtWriteMask_GotFocus(object sender, RoutedEventArgs e)
    {
      ucKeyboard._txInput = (TextBox)sender;
      ucKeyboard.Show();
    }

    private void txtWriteMask_LostFocus(object sender, RoutedEventArgs e)
    {
      ucKeyboard.Hide();
    }

    private void txtTagPassword_GotFocus(object sender, RoutedEventArgs e)
    {
      ucKeyboard._txInput = (TextBox)sender;
      ucKeyboard.Show();
    }

    private void txtTagPassword_LostFocus(object sender, RoutedEventArgs e)
    {
      ucKeyboard.Hide();
    }

    private void txtWriteMask_TextChanged(object sender, TextChangedEventArgs e)
    {
      App.AdminStg.WriteMask = txtWriteMask.Text;
      App.WriteAdminSettings();
    }

    private void txtTagPassword_TextChanged(object sender, TextChangedEventArgs e)
    {
      // Controllo che non ci siano caratteri NON HEX
      for (int k = txtTagPassword.Text.Length-1; k>=0 ; k--)
      {
        if ((txtTagPassword.Text[k] >= 'A' && txtTagPassword.Text[k] <= 'F') ||
              (txtTagPassword.Text[k] >= '0' && txtTagPassword.Text[k] <= '9'))
        {
        }
        else
        {
          // Carattere NON VALIDO
          txtTagPassword.Text = txtTagPassword.Text.Remove(k, 1);
          k--;
        }

      }

      App.AdminStg.TagPassword = txtTagPassword.Text;
      App.WriteAdminSettings();
    }

    

  }
}
