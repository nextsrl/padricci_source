﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace PadRicci.Views
{
  /// <summary>
  /// Logica di interazione per AdminSettings_Page2.xaml
  /// </summary>
  public partial class AdminSettings_Page2 : UserControl
  {
    static App App = (Application.Current as App);

    public AdminSettings_Page2()
    {
      InitializeComponent();

      // Inizializzo gli switch
      if (App.AdminStg.ReadAfterWrite == "Y")
        swcReadAfterWrite.IsChecked = true;
      else
        swcReadAfterWrite.IsChecked = false;

      if (App.AdminStg.AlwaysRewrite == "Y")
        swcAlwaysRewrite.IsChecked = true;
      else
        swcAlwaysRewrite.IsChecked = false;

      if (App.AdminStg.EnableRewriteOption == "Y")
        swcEnableRewriteOption.IsChecked = true;
      else
        swcEnableRewriteOption.IsChecked = false;

      // Imposto i valori a video
      if (App.AdminStg.FileLog == "Y")
        swcLogFile.IsChecked = true;
      else
        swcLogFile.IsChecked = false;

      if (App.AdminStg.DBLog == "Y")
        swcLogDB.IsChecked = true;
      else
        swcLogDB.IsChecked = false;

      txtDBConnectionString.Text = App.AdminStg.DBLogConnectionString;
    }

    private void swcChecked(object sender, RoutedEventArgs e)
    {
      ToggleSwitch.HorizontalToggleSwitch ts = (ToggleSwitch.HorizontalToggleSwitch)sender;

      switch (ts.Tag.ToString())
      {
        case "READAFTERWRITE":
          if (ts.IsChecked)
            App.AdminStg.ReadAfterWrite = "Y";
          else
            App.AdminStg.ReadAfterWrite = "N";
          break;
        case "ALWAYS_REWRITE":
          if (ts.IsChecked)
          {
            App.AdminStg.AlwaysRewrite = "Y";
            swcEnableRewriteOption.IsChecked = false;
          }
          else
          {
            App.AdminStg.AlwaysRewrite = "N";
          }
          break;
        case "ENABLE_REWRITE_OPTION":
          if (ts.IsChecked)
            App.AdminStg.EnableRewriteOption = "Y";
          else
            App.AdminStg.EnableRewriteOption = "N";
          break;
        case "LOGFILE":
          if (ts.IsChecked)
            App.AdminStg.FileLog = "Y";
          else
            App.AdminStg.FileLog = "N";
          break;
        case "LOGDB":
          if (ts.IsChecked)
            App.AdminStg.DBLog = "Y";
          else
            App.AdminStg.DBLog = "N";
          break;
      }
      App.WriteAdminSettings();
    }

    #region Metodi txtDBConnectionString
    private void txtDBConnectionString_GotFocus(object sender, RoutedEventArgs e)
    {
      ucKeyboard._txInput = (TextBox)sender;
      ucKeyboard.Show();
    }

    private void txtDBConnectionString_LostFocus(object sender, RoutedEventArgs e)
    {
      ucKeyboard.Hide();
    }

    private void txtDBConnectionString_TextChanged(object sender, TextChangedEventArgs e)
    {
      App.AdminStg.DBLogConnectionString = txtDBConnectionString.Text;
      App.WriteAdminSettings();
    }
    #endregion

    #region Metodi txtDBInsertCommand
    private void txtDBInsertCommand_GotFocus(object sender, RoutedEventArgs e)
    {
      ucKeyboard._txInput = (TextBox)sender;
      ucKeyboard.Show();
    }

    private void txtDBInsertCommand_LostFocus(object sender, RoutedEventArgs e)
    {
      ucKeyboard.Hide();
    }

    private void txtDBInsertCommand_TextChanged(object sender, TextChangedEventArgs e)
    {
      App.AdminStg.DBInsertCommand = txtDBInsertCommand.Text;
      App.WriteAdminSettings();
    }
    #endregion
  }
}
