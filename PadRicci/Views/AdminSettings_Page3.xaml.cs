﻿using PadRicci;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;

namespace PadRicci.Views
{
  /// <summary>
  /// Logica di interazione per AdminSettings_Page3.xaml
  /// </summary>
  public partial class AdminSettings_Page3 : UserControl
  {
    static App App = (Application.Current as App);

    public AdminSettings_Page3()
    {
      InitializeComponent();

      txtUserPassword.Text = App.AdminStg.UserPassword;
      txtSupervisorPassword.Text = App.AdminStg.SupervisorPassword;
      txtAdminPassword.Text = App.AdminStg.AdminPassword;
      
      lblVersion.Text = String.Format("Versione software: {0}", App.AssemblyVersion);
    }

    private void Button_Click(object sender, RoutedEventArgs e)
    {
      // Esco dall'applicazione
      Environment.Exit(0);
      
    }

    private void BtnUpdate_Click(object sender, RoutedEventArgs e)
    {
      Utils ut = new Utils();


      //string[] fileList = ut.CheckUpdate();
      List<Models.DownloadFile> FileList = ut.CheckUpdate();

      
      if (FileList != null)
      {
        if (FileList.Count > 0)
        {
          // Trovati Aggiornamenti.
          // Lancio il download degli stessi
          DownloadPage d = new DownloadPage(FileList);
          d.ShowDialog();
        }
        else
        {
          MessageBox.Show("Nessun aggiornamento disponibile.");
        }
      }

      /*
      try
      {
        Process.Start(System.IO.Path.Combine(App.AssemblyDirectory, "NSWUpdater.exe"));
        try
        {
          Process.GetCurrentProcess().Kill();
        }
        catch
        {
          Environment.Exit(0);
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(String.Format("Errore durante il lancio del software di aggiornamento!\n{0}", ex.Message));
      }
      */
    }

    #region Metodi txtUserPassword
    private void txtUserPassword_GotFocus(object sender, RoutedEventArgs e)
    {
      ucKeyboard._txInput = (TextBox)sender;
      ucKeyboard.Show();
    }

    private void txtUserPassword_LostFocus(object sender, RoutedEventArgs e)
    {
      ucKeyboard.Hide();
      App.AdminStg.UserPassword=txtUserPassword.Text;
    }

    private void txtUserPassword_TextChanged(object sender, TextChangedEventArgs e)
    {
      
    }
    #endregion

    #region Metodi txtSupervisor
    private void txtSupervisorPassword_GotFocus(object sender, RoutedEventArgs e)
    {
      ucKeyboard._txInput = (TextBox)sender;
      ucKeyboard.Show();
    }

    private void txtSupervisorPassword_LostFocus(object sender, RoutedEventArgs e)
    {
      ucKeyboard.Hide();
      App.AdminStg.SupervisorPassword=txtSupervisorPassword.Text;
    }

    private void txtSupervisorPassword_TextChanged(object sender, TextChangedEventArgs e)
    {
      
    }
    #endregion

    #region Metodi txtAdminPassword
    private void txtAdminPassword_GotFocus(object sender, RoutedEventArgs e)
    {
      ucKeyboard._txInput = (TextBox)sender;
      ucKeyboard.Show();
    }

    private void txtAdminPassword_LostFocus(object sender, RoutedEventArgs e)
    {
      ucKeyboard.Hide();
      App.AdminStg.AdminPassword= txtAdminPassword.Text;
    }

    private void txtAdminPassword_TextChanged(object sender, TextChangedEventArgs e)
    {
      
    }
    #endregion

    
    
  }
}
