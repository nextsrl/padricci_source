﻿using PadRicci;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;

namespace PadRicci.Views
{
  /// <summary>
  /// Logica di interazione per AdminSettings_Page3.xaml
  /// </summary>
  public partial class AdminSettings_Page4 : UserControl
  {
    static App App = (Application.Current as App);

    private Button activeButton;

    public AdminSettings_Page4()
    {
      InitializeComponent();

      // Recupero dei valori salvati
      if (App.AdminStg.EnableLinkToMorph == "Y")
      {
        swcLinkMorph.IsChecked = true;
      }
      else
      {
        swcLinkMorph.IsChecked = false;
      }

      txtEncCategory01.Text = App.AdminStg.EncodingCategory01;
      txtEncCategory02.Text = App.AdminStg.EncodingCategory02;
      txtEncCategory03.Text = App.AdminStg.EncodingCategory03;
      txtEncCategory04.Text = App.AdminStg.EncodingCategory04;

    }

    private void swcChecked(object sender, RoutedEventArgs e)
    {
      ToggleSwitch.HorizontalToggleSwitch ts = (ToggleSwitch.HorizontalToggleSwitch)sender;

      switch (ts.Tag.ToString())
      {
        case "LINKMORPH":
          if (ts.IsChecked)
            App.AdminStg.EnableLinkToMorph = "Y";
          else
            App.AdminStg.EnableLinkToMorph = "N";
          break;
      }
      App.WriteAdminSettings();
    }

    private void txtEncCategory_Click(object sender, RoutedEventArgs e)
    {
//      activeButton = ((RadioButton)sender).;

      // Accendo la tastiera
      ucKeyboard._txInput = null;
      ucKeyboard._btInput = (Button)sender;
      ucKeyboard.Show();

    }

    private void txtEncCategory_GotFocus(object sender, RoutedEventArgs e)
    {
      ucKeyboard._txInput = (TextBox)sender;
      ucKeyboard._btInput = null;
      ucKeyboard.Show();
    }

    private void txtEncCategory_LostFocus(object sender, RoutedEventArgs e)
    {
      ucKeyboard._txInput = null;
      ucKeyboard.Hide();
    }

    private void txtEncCategory_TextChanged(object sender, TextChangedEventArgs e)
    {
      string cat = ((TextBox)sender).Tag.ToString();

      switch (cat)
      {
        case "ENCCATEGORY01":
          App.AdminStg.EncodingCategory01 = ((TextBox)sender).Text;
          break;

        case "ENCCATEGORY02":
          App.AdminStg.EncodingCategory02 = ((TextBox)sender).Text;
          break;

        case "ENCCATEGORY03":
          App.AdminStg.EncodingCategory03 = ((TextBox)sender).Text;
          break;

        case "ENCCATEGORY04":
          App.AdminStg.EncodingCategory04 = ((TextBox)sender).Text;
          break;

        case "ENCCATEGORY05":
          App.AdminStg.EncodingCategory05 = ((TextBox)sender).Text;
          break;
      }

      App.WriteAdminSettings();
    }
  }
}
