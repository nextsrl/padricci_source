﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace PadRicci
{
  /// <summary>
  /// Logica di interazione per WriteTag.xaml
  /// </summary>
  public partial class WriteTag : Page
  {

    static App App = (Application.Current as App);
    public static DispatcherTimer tmrRefresh = new DispatcherTimer();
    List<App.tagRFID> TagWritten = new List<App.tagRFID>();

    public WriteTag()
    {
      InitializeComponent();

      wrkPlay.WorkerSupportsCancellation = true;
      wrkPlay.DoWork += new DoWorkEventHandler(wrkPlay_DoWork);

      App.TagsToWrite = new List<Impinj.OctaneSdk.Tag>();
      App.TagsWritten = new List<Impinj.OctaneSdk.Tag>();
      App.FormWriteTag = this;

      // L'opzione di riscrittura si accende solo con utilizzo di tag protetti da password
      if (String.IsNullOrEmpty(App.AdminStg.TagPassword))
      {
        brdRewriteTag.Visibility = System.Windows.Visibility.Hidden;
        //sep.Visibility = System.Windows.Visibility.Hidden;
      }
      else
      {
        // Lo switch è visibile se e solo se è attiva l'opzione nelle impostazioni
        brdRewriteTag.Visibility = Visibility.Visible;
        //sep.Visibility = Visibility.Visible;

        if (App.AdminStg.AlwaysRewrite == "Y")
          swcRewritetag.IsChecked = true;
        else
          swcRewritetag.IsChecked = false;

        if (App.AdminStg.EnableRewriteOption == "Y")
          swcRewritetag.IsEnabled = true;
        else
          swcRewritetag.IsEnabled = false;
      }
      
      txtValue.Focus();
      AzzeraValore();
      App.RFIDErrorMessage = "";
      App.WritingState = -1;
      TagWritten.Clear();

      lvTagScritti.ItemsSource = App.LocEPCListWrite;

      // Faccio partire il timer
      //tmrRefresh.Interval = TimeSpan.FromMilliseconds(1000);
      //tmrRefresh.Tick += tmrRefresh_Tick;
      //tmrRefresh.Start();
    }

    public BackgroundWorker wrkPlay = new BackgroundWorker();

    public void PlaySound(string Type)
    {
      wrkPlay.RunWorkerAsync(Type);
    }

    private void wrkPlay_DoWork(object sender, DoWorkEventArgs e)
    {
      if (e.Argument.ToString() == "OK")
      {
        if (App.UserStg.SoundOK == "Y")
        {
          try
          {
            MediaPlayer SoundOK = new MediaPlayer();
            SoundOK.Open(new Uri(System.IO.Path.Combine(
              System.IO.Path.Combine(System.IO.Path.GetDirectoryName(
              System.Reflection.Assembly.GetEntryAssembly().Location), "Sounds"),
                 "writeOK.wav")));
            SoundOK.Volume = 100;

            SoundOK.Play();
            //SoundOK.Close();

          }
          catch (Exception ex)
          {
            Console.WriteLine("Errore: " + ex.Message);
          }
        }
      }
      else
      {
        if (App.UserStg.SoundKO == "Y")
        {
          try
          {
            MediaPlayer SoundKO = new MediaPlayer();
            SoundKO.Open(new Uri(System.IO.Path.Combine(
              System.IO.Path.Combine(System.IO.Path.GetDirectoryName(
              System.Reflection.Assembly.GetEntryAssembly().Location), "Sounds"),
                 "writeKO.wav")));
            SoundKO.Volume = 100;

            SoundKO.Play();
            //SoundOK.Close();

          }
          catch (Exception ex)
          {
            Console.WriteLine("Errore: " + ex.Message);
          }
        }
      }

    }

    public delegate void invokeDelegate();

    public void ViewAlertMessage()
    {
      int WritingState = App.WritingState;

      switch (WritingState)
      {
        case -1:
          imgSemaforo.Visibility = System.Windows.Visibility.Hidden;
          break;
        case 0:
        case 99:
          // Lanciato la scrittura in attesa di completarla
          imgSemaforo.Source = new BitmapImage(
                  new Uri(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\Orange.png"), UriKind.Absolute));
          imgSemaforo.Visibility = System.Windows.Visibility.Visible;

          break;
        case 1:
          // Scrittura OK

          // Reimposto la riscrittura a FALSE a meno che non sia impostata fissa dalle admin options
          try
          {
            if (App.AdminStg.AlwaysRewrite == "Y")
              swcRewritetag.IsChecked = true;
            else
              swcRewritetag.IsChecked = false;
          }
          catch
          {
          }
          // Effettuo la verifica del tag appena letto

          imgSemaforo.Source = new BitmapImage(
                  new Uri(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\Green.png"), UriKind.Absolute));
          imgSemaforo.Visibility = System.Windows.Visibility.Visible;
          StartViewOK();
          if (App.UserStg.SoundOK=="Y")
            PlaySound("OK");

          break;
        case 2:
          // Scrittura KO
          imgSemaforo.Source = new BitmapImage(
                  new Uri(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\Red.png"), UriKind.Absolute));
          imgSemaforo.Visibility = System.Windows.Visibility.Visible;

          StartViewKO(App.RFIDErrorMessage);
          if (App.UserStg.SoundKO=="Y")
            PlaySound("KO");

          break;
        case 7:
          imgSemaforo.Source = new BitmapImage(
                  new Uri(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\Red.png"), UriKind.Absolute));
          imgSemaforo.Visibility = System.Windows.Visibility.Visible;
          StartViewKO(App.RFIDErrorMessage);
          break;
        case 8:
          imgSemaforo.Source = new BitmapImage(
                  new Uri(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\Red.png"), UriKind.Absolute));
          imgSemaforo.Visibility = System.Windows.Visibility.Visible;
          StartViewKO("Posizionare un tag sul piano!");
          break;
        case 9:
          imgSemaforo.Source = new BitmapImage(
                  new Uri(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\Red.png"), UriKind.Absolute));
          imgSemaforo.Visibility = System.Windows.Visibility.Visible;
          StartViewKO(String.Format("AFFOLLAMENTO O MANCANZA TAG \n Trovati: ({0})", App.LocEPCList.Count));
          break;
      }

      txtAlert.Text = App.RFIDErrorMessage;
      lvTagScritti.ItemsSource = App.LocEPCListWrite;
      lvTagScritti.Items.Refresh();

      if (lvTagScritti.Items.Count > 0)
      {
        //        lvTagScritti.Items.
      }

      // {SO} - 05/02/2016
      /*
      if (App.WritingState != 0)
        RunCheck();
      */



    }

    public void ViewAlertAsync()
    {
      Dispatcher.Invoke(new Action(() =>
      {
        ViewAlertMessage();
      }), DispatcherPriority.Normal);
    }

    void tmrRefresh_Tick(object sender, EventArgs e)
    {
      tmrRefresh.IsEnabled = false;
      int WritingState = App.WritingState;

      if (App.OldWritingState != WritingState)
      {
        App.timerCounter = 0;
        App.OldWritingState = WritingState;
      }
      else
      {
        tmrRefresh.IsEnabled = true;
        return;
      }

      switch (WritingState)
      {
        case -1:
          imgSemaforo.Visibility = System.Windows.Visibility.Hidden;
          break;
        case 0:
        case 99:
          // Lanciato la scrittura in attesa di completarla
          imgSemaforo.Source = new BitmapImage(
                  new Uri(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\Orange.png"), UriKind.Absolute));
          imgSemaforo.Visibility = System.Windows.Visibility.Visible;

          break;
        case 1:
          // Scrittura OK

          // Reimposto la riscrittura a FALSE a meno che non sia impostata fissa dalle admin options
          if (App.AdminStg.AlwaysRewrite == "Y")
            swcRewritetag.IsChecked = true;
          else
            swcRewritetag.IsChecked = false;

          // Effettuo la verifica del tag appena letto

          imgSemaforo.Source = new BitmapImage(
                  new Uri(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\Green.png"), UriKind.Absolute));
          imgSemaforo.Visibility = System.Windows.Visibility.Visible;
          StartViewOK();

          break;
        case 2:
          // Scrittura KO
          imgSemaforo.Source = new BitmapImage(
                  new Uri(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\Red.png"), UriKind.Absolute));
          imgSemaforo.Visibility = System.Windows.Visibility.Visible;

          StartViewKO(App.RFIDErrorMessage);
          break;
        case 8:
          imgSemaforo.Source = new BitmapImage(
                  new Uri(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\Red.png"), UriKind.Absolute));
          imgSemaforo.Visibility = System.Windows.Visibility.Visible;
          StartViewKO("Posizionare un tag sul piano!");
          break;
        case 9:
          imgSemaforo.Source = new BitmapImage(
                  new Uri(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), @"Images\Red.png"), UriKind.Absolute));
          imgSemaforo.Visibility = System.Windows.Visibility.Visible;
          StartViewKO(String.Format("AFFOLLAMENTO TAG \n Trovati: ({0})", App.LocEPCList.Count));
          break;
      }

      App.timerCounter++;

      txtAlert.Text = App.RFIDErrorMessage;
      lvTagScritti.ItemsSource = App.LocEPCListWrite;
      lvTagScritti.Items.Refresh();

      if (lvTagScritti.Items.Count > 0)
      {
        //        lvTagScritti.Items.
      }

      // {SO} - 05/02/2016
      /*
      if (App.WritingState != 0)
        RunCheck();
      */

      tmrRefresh.IsEnabled = true;
    }

    /// <summary>
    /// Check controlli
    /// </summary>
    private void RunCheck()
    {
      /*if (App.WritingState == 0)
      {
        // Lanciato la scrittura. In questo stato faccio verifiche
        return;
      }
       * */

      if (App.LocEPCList.Count > App.AdminStg.ConcurrentTag)
      {
        blockAlert.Visibility = System.Windows.Visibility.Visible;
        lblInfoAlert.Text = String.Format("AFFOLLAMENTO TAG \n Trovati: ({0})", App.LocEPCList.Count);
        lblInfoAlert2.Text = "";

        blockAlert.Visibility = System.Windows.Visibility.Hidden;
        txtValue.Text = "";
        txtValue.IsEnabled = false;
        return;
      }

      if (App.LocEPCList.Count > 0 && App.LocEPCList.Count < App.AdminStg.ConcurrentTag)
      {
        blockAlert.Visibility = System.Windows.Visibility.Visible;
        lblInfoAlert.Text = String.Format("MANCANZA TAG \n Trovati: ({0})", App.LocEPCList.Count);
        lblInfoAlert2.Text = "";

        blockAlert.Visibility = System.Windows.Visibility.Hidden;
        txtValue.Text = "";
        txtValue.IsEnabled = false;
        return;
      }


      // {SO} - 05/02/2016
      // Nel caso in cui non siano presenti tag sul piano semplicemente esco
      if (App.LocEPCList.Count == 0)
      {
        blockAlert.Visibility = System.Windows.Visibility.Visible;
        lblInfoAlert.Text = String.Format("Posizionare il tag sul piano");
        lblInfoAlert2.Text = "";
        txtValue.Text = "";
        txtValue.IsEnabled = false;
        return;
      }

      // Controllo Se il TAG presente sul PAD è diverso da quello 
      // codificato precedentemente
      if (App.LocEPCList.Count >= 1)
      {
        if (App.LocEPCList[0].TID == App.LastTIDProcessed && !swcRewritetag.IsChecked)
        {
          blockAlert.Visibility = System.Windows.Visibility.Hidden;
          lblInfoAlert2.Text = String.Format("TAG Appena processato.");
          txtValue.IsEnabled = false;
          return;
        }
      }

      blockAlert.Visibility = System.Windows.Visibility.Hidden;
      lblInfoAlert.Text = "";
      lblInfoAlert2.Text = "";
      txtValue.IsEnabled = true;
      txtValue.Focus();
    }

    private void AzzeraConteggioTag()
    {
      App.LocEPCList.Clear();
      App.LocEPCListWrite.Clear();
      lvTagScritti.ItemsSource = App.LocEPCListWrite;
      lvTagScritti.Items.Refresh();

    }

    private void BtnAzzera_Click(object sender, RoutedEventArgs e)
    {
      AzzeraConteggioTag();
    }

    private void AzzeraValore()
    {
      txtValue.Text = "";
      txtValue.Focus();
    }

    private void txtValue_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Enter)
      {
        App.TagsToWrite.Clear();
        App.TagsWritten.Clear();

        // Lancio la scrittura
        if (txtValue.Text.Length > 0)
        {
          // Verifico il valore letto
          if (txtValue.Text.Length != App.AdminStg.ReadBytes)
          {
            AzzeraValore();
            App.WritingState = 7; 
            App.RFIDErrorMessage = "Numero di caratteri NON VALIDO (Valore ammesso: " + App.AdminStg.ReadBytes.ToString() +")";
            ViewAlertMessage();
            return;
          }

          if (App.LocEPCList.Count == 0)
          {
            App.WritingState = 8;
            AzzeraValore();
            ViewAlertMessage();
            return;
          }
          if (App.LocEPCList.Count > 0 && App.LocEPCList.Count < App.AdminStg.ConcurrentTag)
          {
            App.WritingState = 9;
            AzzeraValore();
            ViewAlertMessage();
            return;
          }

          if (App.LocEPCList.Count >  App.AdminStg.ConcurrentTag)
          {
            App.WritingState = 9;
            AzzeraValore();
            ViewAlertMessage();
            return;
          }

          // {SO} - 05/02/2016
          App.EPCToWrite = txtValue.Text;
          
          // V1.0.2
          // L'opzione rewriteTag viene abilitata manualmente o tramite i settings amministrativi
          App.Rewritetag = swcRewritetag.IsChecked | App.AdminStg.AlwaysRewrite=="Y";

          // V.1.0.3
          App.ResetTag = false;
          App.SkipPwd = false;

          App.WritingState = 0; // Pronto per la scrittura
          App.RFIDErrorMessage = "";
          ViewAlertAsync();

          //RFIDMonitor.WriteEPCData(App.LastTag, txtValue.Text, swcRewritetag.IsChecked);
          AzzeraValore();
        }
      }
    }

    private void txtValue_GotFocus(object sender, RoutedEventArgs e)
    {
      txtValue.Text = "";
    }

    /* Gestione Visualizzazione Evento OK di scrittura */
    #region VisualizzazioneOK
    public void StartViewOK()
    {
      frmWriteResultOK.Visibility = System.Windows.Visibility.Visible;
      Storyboard sb = new Storyboard();
      DoubleAnimation FadeIn = new DoubleAnimation(0, 0.8, new Duration(new TimeSpan(0, 0, 0, 0, 200)));
      Storyboard.SetTargetProperty(FadeIn, new PropertyPath(Border.OpacityProperty)); //Do not miss the '(' and ')'      
      sb.Children.Add(FadeIn);
      sb.Completed += StartViewOKFadeIn_Completed;
      frmWriteResultOK.BeginStoryboard(sb);
    }

    void StartViewOKFadeIn_Completed(object sender, EventArgs e)
    {
      Storyboard sb = new Storyboard();
      DoubleAnimation StandBy = new DoubleAnimation(0.8, 0.8, new Duration(new TimeSpan(0, 0, 0, 0, 600)));
      Storyboard.SetTargetProperty(StandBy, new PropertyPath(Border.OpacityProperty)); //Do not miss the '(' and ')'      
      sb.Children.Add(StandBy);
      sb.Completed += StartViewOKStandBy_Completed;
      frmWriteResultOK.BeginStoryboard(sb);
    }

    void StartViewOKStandBy_Completed(object sender, EventArgs e)
    {
      Storyboard sb = new Storyboard();
      DoubleAnimation FadeOut = new DoubleAnimation(0.8, 0, new Duration(new TimeSpan(0, 0, 0, 0, 200)));
      Storyboard.SetTargetProperty(FadeOut, new PropertyPath(Border.OpacityProperty)); //Do not miss the '(' and ')'      
      sb.Children.Add(FadeOut);
      sb.Completed += StartViewOKFadeOut_Completed;
      frmWriteResultOK.BeginStoryboard(sb);
    }
    void StartViewOKFadeOut_Completed(object sender, EventArgs e)
    {
      frmWriteResultOK.Visibility = System.Windows.Visibility.Hidden;
    }

    #endregion

    /* Gestione Visualizzazione Evento KO di scrittura */
    #region VisualizzazioneKO

    public void StartViewKO(string KOMessage)
    {
      lblInfoAlert.Text = KOMessage;
      lblKOMessage.Text = KOMessage;
      frmWriteResultKO.Visibility = System.Windows.Visibility.Visible;
      Storyboard sb = new Storyboard();
      DoubleAnimation FadeIn = new DoubleAnimation(0, 0.8, new Duration(new TimeSpan(0, 0, 0, 0, 300)));
      Storyboard.SetTargetProperty(FadeIn, new PropertyPath(Border.OpacityProperty)); //Do not miss the '(' and ')'      
      sb.Children.Add(FadeIn);
      sb.Completed += StartViewKOFadeIn_Completed;
      frmWriteResultKO.BeginStoryboard(sb);
    }

    void StartViewKOFadeIn_Completed(object sender, EventArgs e)
    {
      Storyboard sb = new Storyboard();
      DoubleAnimation StandBy = new DoubleAnimation(0.8, 0.8, new Duration(new TimeSpan(0, 0, 0, 0, 800)));
      Storyboard.SetTargetProperty(StandBy, new PropertyPath(Border.OpacityProperty)); //Do not miss the '(' and ')'      
      sb.Children.Add(StandBy);
      sb.Completed += StartViewKOStandBy_Completed;
      frmWriteResultKO.BeginStoryboard(sb);
    }

    void StartViewKOStandBy_Completed(object sender, EventArgs e)
    {
      Storyboard sb = new Storyboard();
      DoubleAnimation FadeOut = new DoubleAnimation(0.8, 0, new Duration(new TimeSpan(0, 0, 0, 0, 200)));
      Storyboard.SetTargetProperty(FadeOut, new PropertyPath(Border.OpacityProperty)); //Do not miss the '(' and ')'      
      sb.Children.Add(FadeOut);
      sb.Completed += StartViewKOFadeOut_Completed;
      frmWriteResultKO.BeginStoryboard(sb);
    }
    void StartViewKOFadeOut_Completed(object sender, EventArgs e)
    {
      frmWriteResultKO.Visibility = System.Windows.Visibility.Hidden;
    }


    #endregion

    private void swcRewritetag_Checked(object sender, RoutedEventArgs e)
    {
      if (App.AdminStg.SupervisorPassword != "" && App.AdminStg.AlwaysRewrite=="N")
      {
        swcRewritetag.Checked -= swcRewritetag_Checked;
        swcRewritetag.IsChecked = false;

        // Lancio il controllo PASSWORD
        popUp.IsOpen = true;
        txtPassword.Password = "";
      }
    }

    private void KeyboardButton_Click(object sender, RoutedEventArgs e)
    {
      // Funzione di gestione della tastiera
      Button btn = (Button)sender;

      if (btn.Tag != null)
      {
        string tag = btn.Tag.ToString();

        switch (tag)
        {
          case "CANCEL":
            // Chiudo il popup
            popUp.IsOpen = false;
            swcRewritetag.IsChecked = false;
            swcRewritetag.Checked += swcRewritetag_Checked;
            txtValue.Focus();
            break;
          case "X":
            txtPassword.Password = "";
            txtErrorMessage.Text = "";
            break;
          case "OK":
            // Controllo la password inserita
            if (txtPassword.Password == App.AdminStg.SupervisorPassword)
            {
              //Chiudo il popup e sblocco la riscrittura del tag
              popUp.IsOpen = false;
              swcRewritetag.IsChecked = true;
              swcRewritetag.Checked += swcRewritetag_Checked;
              txtValue.Focus();
            }
            else
            {
              txtErrorMessage.Text = "Password errata!";
            }
            break;
          default:
            txtPassword.Password += btn.Tag.ToString();
            break;
        }
      }
    }

    void BtnUP_Click(object sender, RoutedEventArgs e)
    {
      var scrollViewer = App.GetScrollViewer(lvTagScritti) as ScrollViewer;
      if (scrollViewer != null)
      {
        scrollViewer.PageUp();
      }
    }

    void BtnDOWN_Click(object sender, RoutedEventArgs e)
    {
      var scrollViewer = App.GetScrollViewer(lvTagScritti) as ScrollViewer;
      if (scrollViewer != null)
      {
        scrollViewer.PageDown();
      }
    }

    // V1.0.2
    void BtnResetTag_Click(object sender, RoutedEventArgs e)
    {
      App.TagsToWrite.Clear();
      App.TagsWritten.Clear();

      App.LastTIDProcessed = "";
      App.EPCToWrite = "".PadLeft(App.AdminStg.BytesToWrite,'0'); // Inserire nei settings! {SO}

      App.Rewritetag = false;
      App.ResetTag = true;
      App.SkipPwd = false; // La prima volta provo con la pwd (se settata)

      App.WritingState = 0; // Pronto per la scrittura
      App.RFIDErrorMessage = "";
      ViewAlertAsync();

      AzzeraValore();
    }

    private void cmdActualCategory_Click(object sender, RoutedEventArgs e)
    {
      if (grdCategoryList.Visibility != Visibility.Visible)
      {
        ShowCategoryList();
      }
      else
      {
        HideCategoryList();
      }
    }

    private void ShowCategoryList()
    { 
      // Apro l'elenco delle categorie....
      grdCategoryList.Visibility = Visibility.Visible;

      // Lancio l'animazione per portare il margine inferiore da -150 a 0;
      Storyboard sb = new Storyboard();
      DoubleAnimation ScrollIn = new DoubleAnimation(50, 250, new Duration(new TimeSpan(0, 0, 0, 0, 300)));
      Storyboard.SetTargetProperty(ScrollIn, new PropertyPath(StackPanel.HeightProperty)); 
      sb.Children.Add(ScrollIn);
      //sb.Completed += CategoryScrollIn_Completed;
      grdCategoryList.BeginStoryboard(sb);

    }

    private void HideCategoryList()
    {
      // Nascondo l'elenco delle categorie....
      
      // Lancio l'animazione per portare il margine inferiore da -150 a 0;
      Storyboard sb = new Storyboard();
      DoubleAnimation ScrollOut = new DoubleAnimation(250, 50, new Duration(new TimeSpan(0, 0, 0, 0, 300)));
      Storyboard.SetTargetProperty(ScrollOut, new PropertyPath(StackPanel.HeightProperty));
      sb.Children.Add(ScrollOut);
      sb.Completed += CategoryScrollOut_Completed;
      grdCategoryList.BeginStoryboard(sb);

    }

    private void CategoryScrollOut_Completed(object sender, EventArgs e)
    {
      grdCategoryList.Visibility = Visibility.Hidden;
    }

    private void Page_Loaded(object sender, RoutedEventArgs e)
    {
      // Imagino il layout delle categorie
      SetCategoryLayout();
    }

    private void SetCategoryLayout()
    {
      if (App.AdminStg.EncodingCategory01.Length > 0)
      {
        txtCategoryItem01.Text = App.AdminStg.EncodingCategory01;
        cmdCategoryItem01.Visibility = Visibility.Visible;
      } 
      else
      {
        cmdCategoryItem01.Visibility = Visibility.Hidden;
      }

      if (App.AdminStg.EncodingCategory02.Length > 0)
      {
        txtCategoryItem02.Text = App.AdminStg.EncodingCategory02;
        cmdCategoryItem02.Visibility = Visibility.Visible;
      }
      else
      {
        cmdCategoryItem02.Visibility = Visibility.Hidden;
      }


      if (App.AdminStg.EncodingCategory03.Length > 0)
      {
        txtCategoryItem03.Text = App.AdminStg.EncodingCategory03;
        cmdCategoryItem03.Visibility = Visibility.Visible;
      }
      else
      {
        cmdCategoryItem03.Visibility = Visibility.Hidden;
      }


      if (App.AdminStg.EncodingCategory04.Length > 0)
      {
        txtCategoryItem04.Text = App.AdminStg.EncodingCategory04;
        cmdCategoryItem04.Visibility = Visibility.Visible;
      }
      else
      {
        cmdCategoryItem04.Visibility = Visibility.Hidden;
      }

      // Importo il valore di default con la categoria 01:
      cmdActualCategory.Tag = App.AdminStg.EncodingCategory01;
      ((TextBlock)cmdActualCategory.Content).Text = App.AdminStg.EncodingCategory01;
      ((TextBlock)cmdActualCategory.Content).Background = txtCategoryItem01.Background;

    }

    private void cmdCategoryItem_Click(object sender, RoutedEventArgs e)
    {
      // Selezionata un'altra categoria.
      Button btn = ((Button)sender);
      TextBlock tb = (TextBlock)btn.Content;
      TextBlock tbTarget = (TextBlock)cmdActualCategory.Content;

      cmdActualCategory.Tag = btn.Tag;
      tbTarget.Text = tb.Text;
      tbTarget.Background = tb.Background;

      HideCategoryList();

    }
  }
}
